﻿using System.IO;
using System.Threading.Tasks;
using Autofac;
using Client.Contract.Echo;
using Client.Service.Model.Outgoing.Echo;
using Client.Service.Tests.Common;
using Client.Service.Tests.Mock.Handler;
using MassTransit;
using MediatR;
using Xunit;
using Xunit.Sdk;

//namespace Client.Service.Tests.Echo
//{
//    public class EchoTests : BaseFixture
//    {
//        [Fact]
//        public void TestRouteMap_Success()
//        {
//            var path = System.AppDomain.CurrentDomain.BaseDirectory;
//            var file = File.OpenText($"{path}/Data/Echo_Success.json").ReadToEnd();

//            var routeMap = Container.Resolve<IRouteMap>();
//            var option = routeMap.Map(file);
//            option.Match(
//                some: x => Assert.NotNull(x as IEchoSuccessNotification),
//                none: x => throw new TestClassException("Should succeed"));
//        }
//        [Fact]
//        public void TestRouteMap_Error()
//        {
//            var path = System.AppDomain.CurrentDomain.BaseDirectory;
//            var file = File.OpenText($"{path}/Data/Echo_Error.json").ReadToEnd();

//            var routeMap = Container.Resolve<IRouteMap>();
//            var option = routeMap.Map(file);
//            option.Match(
//                some: x => Assert.NotNull(x as IEchoErrorNotification),
//                none: x => throw new TestClassException("Should succeed"));
//        }
//        [Fact]
//        public async Task HandleRequest()
//        {
//            IEchoRequest loginRequest = new EchoRequest()
//            {
//                CorrelationId = NewId.NextGuid(),
//                Message = "echo"
//            };
//            var handler = Container.Resolve<MockEchoSuccessHandler>();

//            var count = handler.Messages.Count;
//            var mediator = Container.Resolve<IMediator>();
//            await mediator.Send(loginRequest);

//            Assert.Equal(count + 1, handler.Messages.Count);

//        }
//    }
//}
