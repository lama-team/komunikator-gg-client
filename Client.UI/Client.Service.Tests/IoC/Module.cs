﻿using Autofac;
using MediatR;

namespace Client.Service.Tests.IoC {
    public class Module : Autofac.Module{
        protected override void Load(ContainerBuilder builder) {
            base.Load(builder);
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>)).AsImplementedInterfaces().SingleInstance().AsSelf();
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IAsyncNotificationHandler<>)).AsImplementedInterfaces().SingleInstance().AsSelf();
        }
    }
}
