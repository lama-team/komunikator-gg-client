﻿using System.Collections.Generic;
using MediatR;

namespace Client.Service.Tests.Mock {
    public class BaseMock<T> : INotificationHandler<T> where T : INotification {
        public List<T> Messages { get; set; } = new List<T>();
        public void Handle(T notification) {
            Messages.Add(notification);
        }
    }
}
