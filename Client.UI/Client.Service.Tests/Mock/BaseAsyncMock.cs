﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;

namespace Client.Service.Tests.Mock {
    public class BaseAsyncMock<T> : IAsyncNotificationHandler<T> where T : INotification {
        public List<T> Messages { get; set; } = new List<T>();
        public async Task Handle(T notification) {
            Messages.Add(notification);
            await Task.CompletedTask;
        }
    }
}
