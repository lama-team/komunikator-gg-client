﻿using Client.Contract.Echo;

namespace Client.Service.Tests.Mock.Handler {
    public class MockEchoSuccessHandler : BaseMock<IEchoSuccessNotification> {
    }
    public class MockEchoErrorHandler : BaseMock<IEchoErrorNotification> {
    }
}
