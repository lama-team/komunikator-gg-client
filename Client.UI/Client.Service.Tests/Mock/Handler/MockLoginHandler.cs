﻿using Client.Contract.Login;

namespace Client.Service.Tests.Mock.Handler {
    public class MockSuccessLoginHandler : BaseMock<ILoginSuccessNotification> {
    }

    public class MockErrorLoginHandler : BaseMock<ILoginErrorNotification> {
    }
}
