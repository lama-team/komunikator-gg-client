﻿using Client.Contract.Register;

namespace Client.Service.Tests.Mock.Handler {
    public class MockRegisterSuccessHandler : BaseAsyncMock<IRegisterSuccessNotification> {
    }

    public class MockRegisterErrorHandler : BaseAsyncMock<IRegisterErrorNotification> {
    }
}
