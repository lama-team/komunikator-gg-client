﻿using System;
using Autofac;

namespace Client.Service.Tests.Common {
    public abstract class BaseFixture {
        protected IContainer Container { get; set; }

        protected BaseFixture(Action<ContainerBuilder> action = null) {
            var builder = new ContainerBuilder();
            builder.RegisterModule<Service.IoC.Module>();
            builder.RegisterModule<IoC.Module>();
            action?.Invoke(builder);
            Container = builder.Build();
        }
    }
}
