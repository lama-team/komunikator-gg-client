﻿using System.IO;
using System.Threading.Tasks;
using Autofac;
using Client.Contract.Register;
using Client.Service.Model.Outgoing.Register;
using Client.Service.Tests.Common;
using Client.Service.Tests.Mock.Handler;
using MassTransit;
using MediatR;
using Xunit;
using Xunit.Sdk;

//namespace Client.Service.Tests.Register {
//    public class RegisterTests : BaseFixture{
//        [Fact]
//        public void TestRouteMap_Success() {
//            var path = System.AppDomain.CurrentDomain.BaseDirectory;
//            var file = File.OpenText($"{path}/Data/Register_Success.json").ReadToEnd();

//            var routeMap = Container.Resolve<IRouteMap>();
//            var option = routeMap.Map(file);
//            option.Match(
//                some: x => Assert.NotNull(x as IRegisterSuccessNotification),
//                none: x => throw new TestClassException("Should succeed"));
//        }
//        [Fact]
//        public void TestRouteMap_Error() {
//            var path = System.AppDomain.CurrentDomain.BaseDirectory;
//            var file = File.OpenText($"{path}/Data/Register_Error.json").ReadToEnd();

//            var routeMap = Container.Resolve<IRouteMap>();
//            var option = routeMap.Map(file);
//            option.Match(
//                some: x => Assert.NotNull(x as IRegisterErrorNotification),
//                none: x => throw new TestClassException("Should succeed"));
//        }
//        [Fact]
//        public async Task HandleRequest() {
//            IRegisterRequest loginRequest = new RegisterRequest {
//                CorrelationId = NewId.NextGuid(),
//                Login = "test",
//                Password = "password"
//            };
//            var handler = Container.Resolve<MockRegisterSuccessHandler>();

//            var count = handler.Messages.Count;
//            var mediator = Container.Resolve<IMediator>();
//            await mediator.Send(loginRequest);

//            Assert.Equal(count + 1, handler.Messages.Count);

//        }
//    }
//}
