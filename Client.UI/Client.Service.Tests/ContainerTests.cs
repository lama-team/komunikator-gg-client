﻿using Autofac;
using Client.Service.Tests.Common;
using Xunit;

namespace Client.Service.Tests {
    public class ContainerTests : BaseFixture {

    [Fact]
    public void ShouldResolveContainer() {
        Assert.NotNull(Container);
    }

    [Fact]
    public void ShouldResolveRouteMap() {
        var service = Container.Resolve<IRouteMap>();
        Assert.NotNull(service);
    }
    }
}
