﻿using Client.Contract.Core;

namespace Client.Contract.Login {
    public interface ILoginRequest : ICorrelatedRequest{
        string Login { get; set; }
        string Password { get; set; }
    }
}
