﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Login {
    public interface ILoginErrorNotification : IErrorNotification{
    }
}
