﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Login {
    public interface ILoginSuccessNotification : ISuccessNotification{
        string Token { get; set; }
    }
}
