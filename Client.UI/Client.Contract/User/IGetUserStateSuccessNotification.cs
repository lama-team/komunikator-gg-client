﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Contract.Core.Notification;

namespace Client.Contract.User
{
    public interface IGetUserStateSuccessNotification : ISuccessNotification
    {
        Contact.Contact User { get; set; }
        IEnumerable<ContactRequest> ContactRequests { get; set; }
        IEnumerable<ContactWithMessages> Contacts { get; set; }
    }
}
