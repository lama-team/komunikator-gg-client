﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;

namespace Client.Contract.User
{
    public interface ISetUserDataRequest : IAuthRequest
    {
        Contact.Contact Contact { get; set; }
    }
}
