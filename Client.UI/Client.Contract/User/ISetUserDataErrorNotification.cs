﻿using Client.Contract.Core.Notification;

namespace Client.Contract.User
{
    public interface ISetUserDataErrorNotification : IErrorNotification
    {
    }
}
