﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core.Notification;

namespace Client.Contract.User
{
    public interface ISetUserDataSuccessNotification : ISuccessNotification
    {
        Contact.Contact Contact { get; set; }
    }

}
