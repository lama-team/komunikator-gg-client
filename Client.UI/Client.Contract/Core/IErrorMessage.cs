﻿using System;

namespace Client.Contract.Core {
    public interface IErrorMessage : IMessage{
        MessageStatus Status { get; set; }
        Exception Exception { get; set; }
    }
}
