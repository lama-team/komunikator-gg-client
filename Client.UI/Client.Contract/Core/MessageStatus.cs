﻿namespace Client.Contract.Core {
    public enum MessageStatus {
        Ok = 200,
        BadRequest = 400,
        Unauthorized = 401,
        NotFound = 404,
        InternalError = 500,
        ServiceLayerException = 1000
    }
}
