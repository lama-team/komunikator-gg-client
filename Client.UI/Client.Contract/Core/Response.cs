﻿namespace Client.Contract.Core
{
    public static class Response {
        public const string GenericError = "GENERIC_ERROR";

        public const string EchoSuccessResponse = "EchoSuccessResponse";
        public const string EchoErrorResponse = "EchoErrorResponse";

        public const string LoginSuccessResponse = "LoginSuccessResponse";
        public const string LoginErrorResponse = "LoginErrorResponse";

        public const string RegisterSuccessResponse = "RegisterSuccessResponse";
        public const string RegisterErrorResponse = "RegisterErrorResponse";

        public const string GetContactsSuccessResponse = "GetContactsSuccessResponse";
        public const string GetContactsErrorResponse = "GetContactsErrorResponse";

        public const string MessageNotificationConfirm = "MessageNotificationConfirm";
        public const string ContactRequestSuccessResponse = "ContactRequestSuccessResponse";
        public const string ContactRequestErrorResponse = "ContactRequestErrorResponse";

        public const string ContactRequestNotificationConfirm = "ContactRequestNotificationConfirm";

        public const string SetSettingsErrorResponse = "SetSettingsErrorResponse";
        public const string SetSettingsSuccessResponse = "SetSettingsSuccessResponse";

        public const string GetSettingsSuccessResponse = "GetSettingsSuccessResponse";
        public const string GetSettingsErrorResponse = "GetSettingsErrorResponse";

        public const string SendMessageSuccessResponse = "SendMessageSuccessResponse";
        public const string SendMessageErrorResponse = "SendMessageErrorResponse";

        public const string SetUserDataSuccessResponse = "SetUserDataSuccessResponse";
        public const string SetUserDataErrorResponse = "SetUserDataErrorsResponse";

        public const string GetCountriesSuccessResponse = "GetCountriesSuccessResponse";
        public const string GetCountriesErrorResponse = "GetCountriesErrorResponse";

        public const string GetUserStateSuccessResponse = "GetUserStateSuccessResponse";
        public const string GetUserStateErrorResponse = "GetUserStateErrorResponse";

        public const string ContactRequestNotificationConfirmSuccessResponse = "ContactRequestNotificationConfirmSuccessResponse";
        public const string ContactRequestNotificationConfirmErrorResponse = "ContactRequestNotificationConfirmErrorResponse";
    }
}
