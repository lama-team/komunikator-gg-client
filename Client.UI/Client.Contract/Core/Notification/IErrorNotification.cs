﻿using MediatR;

namespace Client.Contract.Core.Notification {
    public interface IErrorNotification : IErrorMessage, INotification{
    }
}
