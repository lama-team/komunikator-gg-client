﻿using MediatR;

namespace Client.Contract.Core.Notification {
    public interface ISuccessNotification : ISuccessMessage, INotification{
    }
}
