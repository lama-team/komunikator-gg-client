﻿namespace Client.Contract.Core {
    public static class Request {
        public const string Login = "LoginRequest";
        public const string Register = "RegisterRequest";
        public const string Echo = "EchoRequest";
        public const string GetContactsRequest = "GetContactsRequest";
        public const string MessageNotificationPush = "MessageNotificationPush";
        public const string ContactRequestRequest = "ContactRequestRequest";
        public const string ContactRequestNotificationPush = "ContactRequestNotificationPush";
        public const string SetSettingsRequest = "SetSettingsRequest";
        public const string GetSettingsRequest = "GetSettingsRequest";
        public const string SendMessageRequest = "SendMessageRequest";
        public const string SetUserDataRequest = "SetUserDataRequest";
        public const string GetCountriesRequest = "GetCountriesRequest";
        public const string GetUserStateRequest = "GetUserStateRequest";
        public const string ContactRequestNotificationConfirm = "ContactRequestNotificationConfirm";
    }
}
