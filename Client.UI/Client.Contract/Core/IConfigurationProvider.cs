﻿using System.Net;

namespace Client.Contract.Core {
    public interface IConfigurationProvider {
        IPAddress ServerIp { get; }
        int ServerPort { get; }
    }
}
