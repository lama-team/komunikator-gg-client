﻿using MediatR;

namespace Client.Contract.Core {
    public interface ICorrelatedRequest : IMessage, IRequest{
    }

    public interface IAuthRequest : ICorrelatedRequest {
        string Token { get; set; }
    }
}
