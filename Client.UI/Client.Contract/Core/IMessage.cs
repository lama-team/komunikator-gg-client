﻿using System;

namespace Client.Contract.Core {
    public interface IMessage {
        Guid CorrelationId { get; set; }
    }
}
