﻿using Client.Contract.Core;

namespace Client.Contract.Register {
    public interface IRegisterRequest : ICorrelatedRequest {
        string Login { get; set; }
        string Password { get; set; }
    }
}
