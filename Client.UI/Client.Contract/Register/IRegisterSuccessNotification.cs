﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Register {
    public interface IRegisterSuccessNotification : ISuccessNotification {
    }
}
