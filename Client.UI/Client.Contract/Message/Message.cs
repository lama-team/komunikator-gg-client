﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Contract.Message
{
    public class Message
    {
        public string MessageId { get; set; }
        public string Content { get; set; }
        public string SourceUserId { get; set; }
        public string TargetUserId { get; set; }
        public bool Read { get; set; }
    }
}
