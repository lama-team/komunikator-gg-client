﻿using System;
using Client.Contract.Core;

namespace Client.Contract.Message
{
    public interface ISendMessageRequest : IAuthRequest
    {
        string TargetUserId { get; set; }
        Guid MessageId { get; set; }
        string Content { get; set; }
    }
}
