﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core.Notification;

namespace Client.Contract.Contact
{
    public interface IContactRequestNotificationConfirmErrorNotification : IErrorNotification
    {
    }
}
