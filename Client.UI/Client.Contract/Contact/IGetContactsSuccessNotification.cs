﻿using System.Collections.Generic;
using Client.Contract.Core.Notification;

namespace Client.Contract.Contact
{
    public interface IGetContactsSuccessNotification : ISuccessNotification {
        IEnumerable<Contact> Contacts { get; set; }
    }
}
