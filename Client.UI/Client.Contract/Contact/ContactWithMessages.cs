﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Contract.Contact
{
    public class ContactWithMessages
    {
        public Contact Contact { get; set; }
        public IEnumerable<Message.Message> Messages { get; set; }
    }
}
