﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;

namespace Client.Contract.Contact
{
    public interface IContactRequestNotificationConfirm : IAuthRequest
    {
        string SourceUserId { get; set; }
        bool Confirm { get; set; }
    }
}
