﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Contract.Contact
{
    public class ContactRequest {
        public string SourceUserId { get; set; }
        public string TargetUserId { get; set; }
        public bool Accepted { get; set; }
    }
}
