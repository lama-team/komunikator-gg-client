﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;

namespace Client.Contract.Contact
{
    public interface IGetContactsRequest : IAuthRequest
    {
        string UserName { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Country { get; set; }
        string City { get; set; }
        uint? MinAge { get; set; }
        uint? MaxAge { get; set; }
        string Gender { get; set; }
    }
}
