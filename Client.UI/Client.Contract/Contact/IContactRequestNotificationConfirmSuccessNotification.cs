﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Contact
{
    public interface IContactRequestNotificationConfirmSuccessNotification : ISuccessNotification
    {
    }
}
