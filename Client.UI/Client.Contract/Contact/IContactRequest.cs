﻿using Client.Contract.Core;

namespace Client.Contract.Contact
{
    public interface IContactRequest : IAuthRequest {
        string TargetUserId { get; set; }
    }
}
