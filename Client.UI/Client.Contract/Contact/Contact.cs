﻿namespace Client.Contract.Contact {
    public class Contact {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public byte[] Avatar { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public uint Age { get; set; }
        public string Gender { get; set; }
    }
}