﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Echo {
    public interface IEchoErrorNotification : IErrorNotification {
    }
}
