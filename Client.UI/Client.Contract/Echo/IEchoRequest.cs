﻿using Client.Contract.Core;

namespace Client.Contract.Echo {
    public interface IEchoRequest : ICorrelatedRequest{
        string Message { get; set; }
    }
}
