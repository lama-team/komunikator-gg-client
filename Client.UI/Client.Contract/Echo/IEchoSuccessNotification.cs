﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Echo {
    public interface IEchoSuccessNotification : ISuccessNotification {
        string Message { get; set; }
    }
}
