﻿using System.Collections.Generic;
using Client.Contract.Core.Notification;

namespace Client.Contract.Settings
{
    public interface IGetSettingsSuccessNotification : ISuccessNotification
    {
        Dictionary<string,string> Settings { get; set; }
    }
}
