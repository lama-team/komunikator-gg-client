﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;

namespace Client.Contract.Settings
{
    public interface ISetSettingsRequest : IAuthRequest
    {
        Dictionary<string,string> Settings { get; set; }
    }
}
