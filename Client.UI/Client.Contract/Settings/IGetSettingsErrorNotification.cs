﻿using Client.Contract.Core.Notification;

namespace Client.Contract.Settings
{
    public interface IGetSettingsErrorNotification : IErrorNotification
    {
    }
}
