﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core.Notification;

namespace Client.Contract.Countries
{
    public interface IGetCountriesSuccessNotification : ISuccessNotification
    {
        IEnumerable<string> Countries { get; set; }
    }
}
