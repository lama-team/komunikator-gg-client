﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Client.Service;
using Client.Service.Model.Incoming.Echo;
using Client.Service.Model.Incoming.Login;
using Client.Service.Model.Incoming.Register;
using Client.Service.Model.Outgoing.Contact;
using Client.Service.Model.Outgoing.Echo;
using Client.Service.Model.Outgoing.Login;
using Client.Service.Model.Outgoing.Message;
using Client.Service.Model.Outgoing.Register;
using Client.Service.Model.Outgoing.User;
using MediatR;

namespace Client.Console
{

    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<Service.IoC.Module>();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsClosedTypesOf(typeof(INotificationHandler<>)).AsImplementedInterfaces(); // via assembly scan
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .AsClosedTypesOf(typeof(IAsyncNotificationHandler<>)).AsImplementedInterfaces();
            var container = builder.Build();
            var client = container.Resolve<IClient>();
            //var thread = client.StartOnNewThread();
            var mediator = container.Resolve<IMediator>();
            while (true) {
                System.Console.WriteLine("Specify echo message");
                var message = System.Console.ReadLine();
                if (message.Contains("register")) {
                    var arguments = message.Split(' ');

                    mediator.Send(new RegisterRequest {
                        Login = arguments[1],
                        CorrelationId = Guid.NewGuid(),
                        Password = arguments[2]
                    }).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("login")) {
                    var arguments = message.Split(' ');

                    mediator.Send(new LoginRequest
                    {
                        Login = arguments[1],
                        CorrelationId = Guid.NewGuid(),
                        Password = arguments[2]
                    }).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("getState")) {
                    var arguments = message.Split(' ');

                    mediator.Send(new GetUserStateRequest {
                        CorrelationId = Guid.NewGuid(),
                        Token = State.Token
                    }).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("contact all")) {
                    var arguments = message.Split(' ');
                    var model = new GetContactsRequest {
                        CorrelationId = Guid.NewGuid(),
                        Token = State.Token ?? ""
                    };
                    mediator.Send(model).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("contact add"))
                {
                    var arguments = message.Split(' ');

                    mediator.Send(new ContactRequest
                    {
                        CorrelationId = Guid.NewGuid(),
                        Token = State.Token ?? "",
                        TargetUserId = arguments[2]
                    }).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("contact accept"))
                {
                    var arguments = message.Split(' ');

                    mediator.Send(new ContactRequestNotificationConfirm
                    {
                        CorrelationId = Guid.NewGuid(),
                        Token = State.Token,
                        Confirm = true,
                        SourceUserId = arguments[2]
                    }).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("message send")) {
                    var arguments = message.Split(' ');
                    var model = new SendMessageRequest {
                        CorrelationId = Guid.NewGuid(),
                        Token = State.Token,
                        TargetUserId = arguments[2],
                        Content = string.Join(String.Empty, arguments.Skip(3).ToList()),
                        MessageId = Guid.NewGuid()
                    };
                    mediator.Send(model).Wait(TimeSpan.FromSeconds(5));
                    continue;
                }
                if (message.Contains("contact list")) {
                    if (State.UserState?.Contacts != null)
                        foreach (var contact in State.UserState.Contacts) {
                            System.Console.WriteLine($"{contact.Contact.UserId}");
                        }
                    continue;
                }
                if (message.Contains("message list"))
                {
                    if (State.UserState?.Contacts != null)
                        foreach (var contact in State.UserState.Contacts)
                        {
                            System.Console.WriteLine($"{contact.Contact.UserId}");
                            foreach (var m in contact.Messages) {
                                System.Console.WriteLine($"{m.SourceUserId} - {m.Content}");
                            }
                        }
                    continue;
                }
                if (message.Length > 0) {
                    mediator.Send(new EchoRequest {
                        CorrelationId = Guid.NewGuid(),
                        Message = message
                    }).Wait(TimeSpan.FromSeconds(5));
                }
            }
        }
    }
}
