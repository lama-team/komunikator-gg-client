﻿using System;
using Client.Contract.User;
using Client.Service.Model.Incoming;
using Client.Service.Model.Incoming.Contact;
using Client.Service.Model.Incoming.Echo;
using Client.Service.Model.Incoming.Login;
using Client.Service.Model.Incoming.Message;
using Client.Service.Model.Incoming.Register;
using Client.Service.Model.Incoming.User;
using MediatR;

namespace Client.Console
{
    public static class State {
        public static string Token;
        public static IGetUserStateSuccessNotification UserState;
    }

    class GlobalErrorHandler : INotificationHandler<GenericErrorNotification> {
        public void Handle(GenericErrorNotification notification) {
            System.Console.WriteLine($"GENERIC ERROR, {notification.CorrelationId}, {notification.Status} : {notification.Exception.Message}" );
        }
    }
    class EchoReceiver : INotificationHandler<EchoSuccessNotification>
    {
        public void Handle(EchoSuccessNotification notification)
        {
            System.Console.WriteLine("Success: " + notification.Message);
        }
    }

    class EchoErrorReceiver : INotificationHandler<EchoErrorNotification>
    {
        public void Handle(EchoErrorNotification notification)
        {
            System.Console.WriteLine("Error: " + notification.Exception.Message);
        }
    }

    class LoginSuccessReceiver : INotificationHandler<LoginSuccessNotification>
    {
        public void Handle(LoginSuccessNotification notification) {
            State.Token = notification.Token;
            System.Console.WriteLine("Login succeded, token: " + notification.Token);
        }
    }

    class LoginErrorReceiver : INotificationHandler<LoginErrorNotification>
    {
        public void Handle(LoginErrorNotification notification)
        {
            System.Console.WriteLine("Login error: " + notification.Exception.Message);
        }
    }

    class RegisterSuccessReceiver : INotificationHandler<RegisterSuccessNotification>
    {
        public void Handle(RegisterSuccessNotification notification)
        {
            System.Console.WriteLine("Register succeded");
        }
    }

    class RegisterErrorReceiver : INotificationHandler<RegisterErrorNotification>
    {
        public void Handle(RegisterErrorNotification notification)
        {
            System.Console.WriteLine("Register error: " + notification.Exception.Message);
        }
    }

    class UserStateReceiver : INotificationHandler<GetUserStateSuccessNotification> {
        public void Handle(GetUserStateSuccessNotification notification) {
            State.UserState = notification;
            //System.Console.WriteLine("User state succeded");
        }
    }

    class UserStateErrorReceiver : INotificationHandler<GetUserStateErrorNotification> {
        public void Handle(GetUserStateErrorNotification notification) {
            System.Console.WriteLine("User state error");
        }
    }

    class ContactRequestReceiver : INotificationHandler<ContactRequestSuccessNotification> {
        public void Handle(ContactRequestSuccessNotification notification) {
            System.Console.WriteLine("Contact request succeeded");
        }
    }

    class ContactRequestErrorReceiver : INotificationHandler<ContactRequestErrorNotification>
    {
        public void Handle(ContactRequestErrorNotification notification)
        {
            System.Console.WriteLine("Contact request error");
        }
    }

    class GetContactsReceiver : INotificationHandler<GetContactsSuccessNotification> {
        public void Handle(GetContactsSuccessNotification notification) {
            System.Console.WriteLine("Get contacts succeeded");
        }
    }

    class GetContactsErrorReceiver : INotificationHandler<GetContactsErrorNotification>
    {
        public void Handle(GetContactsErrorNotification notification)
        {
            System.Console.WriteLine("Get contacts error");
        }
    }

    class ContactConfirm : INotificationHandler<ContactRequestNotificationConfirmSuccessNotification> {
        public void Handle(ContactRequestNotificationConfirmSuccessNotification notification) {
            System.Console.WriteLine("ContactConfirm succeeded");
        }
    }
    class ContactErrorConfirm : INotificationHandler<ContactRequestNotificationConfirmErrorNotification>
    {
        public void Handle(ContactRequestNotificationConfirmErrorNotification notification)
        {
            System.Console.WriteLine("ContactConfirm error");
        }
    }

    class SendMessage : INotificationHandler<SendMessageSuccessNotification> {
        public void Handle(SendMessageSuccessNotification notification) {
            System.Console.WriteLine("Send message succeeded");
        }
    }

    class SendErrorMessage : INotificationHandler<SendMessageErrorNotification>
    {
        public void Handle(SendMessageErrorNotification notification)
        {
            System.Console.WriteLine("Send message error");
        }
    }
}
