﻿using Autofac;
using Client.UI.ViewModels;
using Client.UI.Windows;
using Hardcodet.Wpf.TaskbarNotification;
using System;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows;

namespace Client.UI {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private TaskbarIcon notifyIcon;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Application.Current.Exit += Current_Exit;
            Container.Instance.Resolve<User>();
            Application.Current.MainWindow = Container.Instance.Resolve<Contacts>();
            
            //create the notifyicon (it's a resource declared in NotifyIconResources.xaml
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");

            Container.Instance.Resolve<LoggingWindow>().ShowWindow();
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            Container.OnExit();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose();
            base.OnExit(e);
        }
    }
}
