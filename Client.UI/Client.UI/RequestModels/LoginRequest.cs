﻿using Client.Contract.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.RequestModels
{
    public class LoginRequest : ILoginRequest
    {
        public LoginRequest(string login, string password)
        {
            Login = login;
            Password = password;
            CorrelationId = Guid.NewGuid();
        }

        public Guid CorrelationId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
