﻿using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.IoC
{
    public class LoggedInTracker : BaseViewModel
    {
        private bool _isLogged;
        public bool IsLogged
        {
            get { return _isLogged; }
            set { _isLogged = value; OnPropertyChanged("IsLogged"); }
        }
    }
}
