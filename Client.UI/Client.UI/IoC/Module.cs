﻿using Autofac;
using Client.UI.Handlers;
using Client.UI.ViewModels;
using Client.UI.Windows;
using MediatR;

namespace Client.UI.IoC{
    public class Module : Autofac.Module{

        protected override void Load(ContainerBuilder builder) {
            base.Load(builder);
            
            builder.RegisterType<LoggingViewModel>().As<LoggingViewModel>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LoggingWindow>().As<LoggingWindow>();

            builder.RegisterType<RegisterViewModel>().As<RegisterViewModel>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RegisterWindow>().As<RegisterWindow>();

            builder.RegisterType<FindNewContactsHandlers>().AsImplementedInterfaces();

            builder.RegisterType<User>().As<User>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<ConversationWindowTracker>().As<ConversationWindowTracker>().SingleInstance();
            builder.RegisterType<LoggedInTracker>().As<LoggedInTracker>().SingleInstance();

            builder.RegisterType<GetStateHandlers>().As<GetStateHandlers>().AsImplementedInterfaces();

            builder.RegisterType<Contacts>().As<Contacts>().SingleInstance();
            builder.RegisterType<ContactsViewModel>().As<ContactsViewModel>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<ConversationWindow>().As<ConversationWindow>().SingleInstance();
            builder.RegisterType<ConversationsViewModel>().As<ConversationsViewModel>().AsImplementedInterfaces().SingleInstance();
        }

    }
}
