﻿using Autofac;
using Client.Service.Model.Incoming.User;
using Client.Service.Model.Outgoing.Contact;
using Client.Service.Model.Outgoing.User;
using Client.UI.Singletons;
using Client.UI.Windows;
using MediatR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Client.UI.ViewModels
{
    public class ContactsViewModel : BaseViewModel
    {
        public User LoggerUser { get; set; }
        public ContactsViewModel(User user)
        {
            LoggerUser = user;
            Settings = new ContactsSettingsViewModel();
            Favourites = new ObservableCollection<Contact>();
            Invitations = new ObservableCollection<PendingContact>();
            Contacts = new ObservableCollection<Contact>();
            Frequent = new ObservableCollection<Contact>();
        }


        private ObservableCollection<Contact> _favourites;
        public ObservableCollection<Contact> Favourites
        {
            get { return _favourites; }
            set { _favourites = value; OnPropertyChanged("Favourites"); }
        }

        private ObservableCollection<PendingContact> _invitations;
        public ObservableCollection<PendingContact> Invitations
        {
            get { return _invitations; }
            set { _invitations = value; OnPropertyChanged("Invitations"); }
        }

        private ObservableCollection<Contact> _contacts;
        public ObservableCollection<Contact> Contacts
        {
            get { return _contacts; }
            set { _contacts = value; OnPropertyChanged("Contacts"); }
        }

        private ObservableCollection<Contact> _frequent;
        public ObservableCollection<Contact> Frequent
        {
            get { return _frequent; }
            set { _frequent = value; OnPropertyChanged("Frequent"); }
        }


        public void ShowConversation(Contact contact)
        {
            ConversationWindowManager.OpenConversation(new Conversation { Name = contact.Name });
        }

        public void AcceptInvitation(Contact contact)
        {
            Container.Mediator.Send(new ContactRequestNotificationConfirm
            {
                CorrelationId = Guid.NewGuid(),
                Token = LoggerUser.Token,
                Confirm = true,
                SourceUserId = contact.Name
            }).Wait(TimeSpan.FromSeconds(5));
            Invitations.Remove(contact as PendingContact);
            Contacts.Add(contact);
        }

        public void RejectInvitation(Contact contact)
        {
            Container.Mediator.Send(new ContactRequestNotificationConfirm
            {
                CorrelationId = Guid.NewGuid(),
                Token = LoggerUser.Token,
                Confirm = false,
                SourceUserId = contact.Name
            }).Wait(TimeSpan.FromSeconds(5));
            Invitations.Remove(contact as PendingContact);
        }

        private void FindNewUser()
        {
            (new FindNewContact(new FindNewContactViewModel(LoggerUser))).ShowDialog();
        }

        public ICommand FindNewUserCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => FindNewUser()
                };
            }
        }

        public ICommand SettingsCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => SettingsWindow.ShowSettingsDialog()
                };
            }
        }

        public ContactsSettingsViewModel Settings { get; set; }
    }
}
