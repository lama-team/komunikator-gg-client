﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.UI.ViewModels
{
    public class PendingContact : Contact
    {
        public PendingContact(ContactsViewModel contacts) : base(contacts)
        {
            Pending = true;
        }

        public ICommand AcceptPendingContact
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => _contacts.AcceptInvitation(this)
                };
            }
        }

        public ICommand RejectPendingContact
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => _contacts.RejectInvitation(this)
                };
            }
        }
    }
}
