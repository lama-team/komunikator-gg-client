﻿using Autofac;
using Client.Contract.User;
using Client.Service.Model.Outgoing.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Contract.Core;
using MediatR;
using Client.Service.Model.Incoming.User;
using System.Windows;
using Client.Service.Model.Incoming.Settings;
using Client.Service.Model.Outgoing.Settings;
using Client.Service.Model.Incoming.Countries;
using Client.Service.Model.Outgoing.Countries;

namespace Client.UI.ViewModels
{
    public class User : BaseViewModel
        , INotificationHandler<GetUserStateSuccessNotification>
        , INotificationHandler<SetUserDataErrorNotification>
        , INotificationHandler<GetSettingsSuccessNotification>
        , INotificationHandler<SetSettingsErrorNotification>
        , INotificationHandler<GetCountriesSuccessNotification>
    {
        bool _updateUserData;

        private void GetUserSettings()
        {
            Settings = new UserSettings();
            _updateUserData = true;
            Container.Mediator.Send(new GetSettingsRequest { CorrelationId = Guid.NewGuid(), Token = Token });
            Container.Mediator.Send(new GetCountriesRequest { CorrelationId = Guid.NewGuid() });
        }

        public void Handle(GetUserStateSuccessNotification notification)
        {
            if (_updateUserData)
            {
                Name = notification.User.FirstName;
                Surname = notification.User.LastName;
                Nickname = notification.User.UserName;
                City = notification.User.City;
                Country = notification.User.Country;
                Male = notification.User.Gender.ToLower() == "m";
                Female = notification.User.Gender.ToLower() == "f";
                Age = notification.User.Age;
                Description = notification.User.Description;
                _updateUserData = false;
            }
        }

        public void Handle(SetUserDataErrorNotification notification)
        {
            MessageBox.Show("Błąd podczas aktualizacji danych. Spróbuj ponownie. " + Environment.NewLine + notification.Exception.Message);
        }

        public void Handle(GetSettingsSuccessNotification notification)
        {
            if (notification.Settings.Any(q => q.Key.ToLower() == "alternativebuttons"))
                Settings.AlternativeCloseMinimalizeButtons =
                    notification.Settings.First(q => q.Key.ToLower() == "alternativebuttons").Value.ToLower() == "true";
        }

        public void Handle(SetSettingsErrorNotification notification)
        {
            MessageBox.Show("Błąd podczas aktualizacji danych. Spróbuj ponownie. " + Environment.NewLine + notification.Exception.Message);
        }

        private string _token;
        public string Token
        {
            get { return _token; }
            set
            {
                if (_token != value)
                {
                    _token = value;
                    GetUserSettings();
                }
            }
        }

        public void UpdateData()
        {
            Container.Mediator.Send(new SetUserDataRequest
            {
                Token = Token,
                CorrelationId = Guid.NewGuid(),
                Contact = new Contract.Contact.Contact
                {
                    UserId = Token,
                    FirstName = Name,
                    LastName = Surname,
                    UserName = Nickname,
                    City = City,
                    Country = Country,
                    Age = Age,
                    Gender = Male ? "m" : "f",
                    Description = Description
                }
            });

            Container.Mediator.Send(new SetSettingsRequest
            {
                CorrelationId = Guid.NewGuid(),
                Token = Token,
                Settings = new Dictionary<string, string>
                  {
                      { "alternativebuttons", Settings.AlternativeCloseMinimalizeButtons ? "true" : "false" }
                  }
            });
        }

        public void Handle(GetCountriesSuccessNotification notification)
        {
            AvailableCountries = notification.Countries.ToList();
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; OnPropertyChanged("Description"); }
        }


        public List<string> AvailableCountries { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Nickname { get; set; }
        public bool Male { get; set; }
        public bool Female { get; set; }
        //public DateTime Birthdate { get; set; }
        public uint Age { get; set; }
        public string Country { get; set; }
        public string City { get; set; }

        public UserSettings Settings { get; set; }


    }
}
