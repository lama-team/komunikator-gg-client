﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Autofac;
using MediatR;
using Client.Contract.Login;
using Client.UI.RequestModels;
using Client.UI.Windows;
using Client.UI.Singletons;

namespace Client.UI.ViewModels
{
    public class LoggingViewModel : BaseViewModel
        , INotificationHandler<ILoginSuccessNotification>
        , INotificationHandler<ILoginErrorNotification>
    {
        public LoggingViewModel() { }
        
        private LoggingWindow _window;
        public LoggingWindow Window
        {
            set
            {
                if (_window == null)
                {
                    _window = value;
                    _window.PasswordPasswordBox.PasswordChanged += new RoutedEventHandler((s, e) => OnPropertyChanged("LoginIsEnabled"));
                }
            }
        }
        
        public bool LoginIsEnabled
        {
            get
            {
                return !String.IsNullOrEmpty(_window.PasswordPasswordBox.Password) &&
                    !String.IsNullOrEmpty(Username);
            }
        }


        private string _username;
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                OnPropertyChanged("Username");
                OnPropertyChanged("LoginIsEnabled");
            }
        }


        public ICommand LoggingCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => LoginIsEnabled,
                    CommandAction = () => SendLoggingRequest()
                };
            }
        }

        private void SendLoggingRequest()
        {
            var mediator = Container.Instance.Resolve<IMediator>();
            mediator.Send(new LoginRequest(Username, _window.PasswordPasswordBox.Password)).Wait();
        }

        public void Handle(ILoginSuccessNotification notification)
        {
            _window.Dispatcher.Invoke((Action)(() =>
            {
                _window.CloseWindow();
            }));
            User user = Container.Instance.Resolve<User>();
            user.Token = notification.Token;
            GetStateWorker.StartWorking(user);
            ContactsWindowManager.Logged(user);
        }

        public void Handle(ILoginErrorNotification notification)
        {
            MessageBox.Show(notification.Exception.Message);
        }
    }
}
