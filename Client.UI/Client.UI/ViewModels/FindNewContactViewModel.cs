﻿using Client.Service.Model.Outgoing.Contact;
using Client.UI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Client.UI.ViewModels
{
    public class FindNewContactViewModel : BaseViewModel
    {
        private User _user;
        private FindNewContact _window;

        public void SetWindow(FindNewContact window)
        {
            _window = window;
        }

        public FindNewContactViewModel(User user)
        {
            _user = user;
        }

        private string _login;
        public string Login
        {
            get { return _login; }
            set { _login = value; OnPropertyChanged("Login");
                OnPropertyChanged("LoginIsEnabled");
            }
        }
        
        private void SendInviteHandler()
        {
            Container.Mediator.Send(new ContactRequest
            {
                CorrelationId = Guid.NewGuid(),
                Token = _user.Token,
                TargetUserId = _login
            }).Wait(TimeSpan.FromSeconds(5));
            _window.Close();
        }

        public bool SendInviteIsEnabled
        {
            get
            {
                return Login != null ? Login.Any() : false;
            }
        }

        public ICommand SendInvite
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => SendInviteIsEnabled,
                    CommandAction = () => SendInviteHandler()
                };
            }
        }
    }
}
