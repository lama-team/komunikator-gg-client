﻿using Client.UI.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.ViewModels
{
    public class ConversationsViewModel : BaseViewModel
    {
        private ConversationWindow _window;

        public ConversationsViewModel(ConversationWindow window)
        {
            _window = window;
            Conversations = new ObservableCollection<Conversation>();
        }

        private ObservableCollection<Conversation> _conversations;
        public ObservableCollection<Conversation> Conversations
        {
            get { return _conversations; }
            set { _conversations = value; OnPropertyChanged("Conversations"); }
        }

        public void CloseConversation(Conversation conversation)
        {
            if (Conversations.Contains(conversation))
                Conversations.Remove(conversation);

            if (DisplayedConversation == conversation)
                DisplayedConversation = Conversations.Any() ? Conversations.Last() : null;

            if (!Conversations.Any()) {
                _window.Hide();
                if (System.Windows.Application.Current.MainWindow != null)
                    System.Windows.Application.Current.MainWindow.Show();
            }
        }

        private Conversation _displayedConversation;
        public Conversation DisplayedConversation
        {
            get { return _displayedConversation; }
            set
            {
                //if (_displayedConversation != null)
                //    _displayedConversation.StopLoadingMessages();
                _displayedConversation = value;
                //_displayedConversation.StartLoadingMessages();
                OnPropertyChanged("DisplayedConversation");
                //TODO: don't use hack, instead use data template https://stackoverflow.com/a/18231374/1585976
                OnPropertyChanged("Messages");
            }
        }

    }
}
