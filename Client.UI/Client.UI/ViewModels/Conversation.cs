﻿using Autofac;
using Client.Service.Model.Outgoing.Message;
using Client.UI.Singletons;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MassTransit;

namespace Client.UI.ViewModels
{
    public class Conversation : BaseViewModel
    {
        public Conversation()
        {
            Messages = new ObservableCollection<Message>();
        }

        private ObservableCollection<Message> _messages;
        public ObservableCollection<Message> Messages
        {
            get { return _messages; }
            set { _messages = value; OnPropertyChanged("Messages"); }
        }

        public string Name { get; set; }

        private string _newMessageText;
        public string NewMessageText
        {
            get { return _newMessageText; }
            set { _newMessageText = value; OnPropertyChanged("NewMessageText"); }
        }
        
        private void Send()
        {
            Messages.Add(new Message { MessageSource = Message.MessageSourceEnum.To, Content = NewMessageText });
            //TODO no jakieś api czy coś, że wiadomość wysłano.
            //
            // OPIS DZIAŁANIA OKIENKA I POBIERANIA WIADOMOŚCI
            //
            // SĄ 2 TRYBY POBRANIA WIADOMOŚCI KONWERSACJI
            //
            // 1) POBIERAMY WSZYSTKIE WIADOMOŚCI - POWIEDZMY Z 15 OSTATNICH WIADOMOŚCI - URUCHAMIANE PRZY URUCHOMIENIU OKNA KONWERSACJI
            //
            // 2) CO SEKUNDĘ IDZIE ZAPYTANIE POBIERAJĄCE TYLKO I WYŁĄCZNIE WIADOMOŚCI OD ROZMÓWCY (TYP WIADOMOŚCI - FROM) ALE W ARGUMENCIE WYSYŁAMY DATĘ 
            // OSTATNIEJ OTRZYMANEJ WIADOMOŚCI OD TEGO ROZMÓWCY, WIĘC POBIERZEMY TYLKO TE NIEZACZYTANE WIADOMOŚCI
            Container.Mediator.Send(new SendMessageRequest
            {
                CorrelationId = Guid.NewGuid(),
                Token = Container.Instance.Resolve<User>().Token,
                TargetUserId = Name,
                Content = string.Join(String.Empty, NewMessageText),
                MessageId = NewId.NextGuid()
            }).Wait(TimeSpan.FromSeconds(5));
            
            NewMessageText = "";
        }

        #region Command delegates

        public ICommand SendMessage
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => !String.IsNullOrEmpty(NewMessageText)
                    ,
                    CommandAction = Send
                };
            }
        }

        public ICommand CloseConversation
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => Container.Instance.Resolve<ConversationsViewModel>().CloseConversation(this)
                };
            }
        }

        public ICommand ShowConverstation
        {
            get
            {
                return new DelegateCommand
                {
                    CommandAction = () => ConversationWindowManager.OpenConversation(this) //Container.Instance.Resolve<ConversationsViewModel>().ShowConversation(this)
                };
            }
        }

        #endregion
    }
}
