﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Client.UI.ViewModels
{
    public class Contact : BaseViewModel
    {
        protected ContactsViewModel _contacts;

        public Contact(ContactsViewModel contacts)
        {
            _contacts = contacts;
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public BitmapImage ProfiPicture { get; set; }

        private bool _pending;
        public bool Pending
        {
            get { return _pending; }
            set { _pending = value; OnPropertyChanged("Pending"); }
        }

        public ICommand ShowConversation
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => !Pending,
                    CommandAction = () => _contacts.ShowConversation(this)
                };
            }
        }
    }
}
