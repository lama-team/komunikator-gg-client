﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.ViewModels
{
    public class Message : BaseViewModel
    {
        public enum MessageSourceEnum
        {
            From //Messege got from some user
            , To //Messege sent to some user
            , Announcement
        }

        public MessageSourceEnum MessageSource { get; set; }

        public string Content { get; set; }


    }
}
