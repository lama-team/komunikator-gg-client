﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.ViewModels
{
    public class ContactsSettingsViewModel : BaseViewModel
    {
        public ContactsSettingsViewModel()
        {
            ShowAll = true;
        }

        private bool _showFrequent;
        public bool ShowFrequent
        {
            get { return _showFrequent; }
            set { _showFrequent = value; OnPropertyChanged("ShowFrequent"); }
        }


        private bool _showAll;
        public bool ShowAll
        {
            get { return _showAll; }
            set { _showAll = value; OnPropertyChanged("ShowAll"); }
        }
    }
}
