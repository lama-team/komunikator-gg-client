﻿using Autofac;
using Client.Contract.Register;
using Client.Service.Model.Outgoing.Register;
using Client.UI.Windows;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Client.UI.ViewModels
{
    public class RegisterViewModel : BaseViewModel
        , INotificationHandler<IRegisterSuccessNotification>
        , INotificationHandler<IRegisterErrorNotification>
    {
        private RegisterWindow _window;
        
        public void SetWindow(RegisterWindow window)
        {
            _window = window;
        }

        private string _login;
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                OnPropertyChanged("Login");
                OnPropertyChanged("RegisterIsEnabled");
            }
        }


        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                OnPropertyChanged("Email");
                OnPropertyChanged("RegisterIsEnabled");
            }
        }

        public bool RegisterIsEnabled
        {
            get
            {
                return !String.IsNullOrEmpty(_window.PasswordBox.Password) &&
                    !String.IsNullOrEmpty(Login) &&
                    !String.IsNullOrEmpty(Email);
            }
        }

        private void SendRegisterRequest()
        {
            Container.Mediator.Send(new RegisterRequest
            {
                Login = Login,
                CorrelationId = Guid.NewGuid(),
                Password = _window.PasswordBox.Password
            });
        }

        public void Handle(IRegisterErrorNotification notification)
        {
            MessageBox.Show(notification.Exception.Message);
        }

        public void Handle(IRegisterSuccessNotification notification)
        {
            _window.Dispatcher.Invoke((Action)(() =>
            {
                _window.CloseWindow();
            }));
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => RegisterIsEnabled,
                    CommandAction = () => SendRegisterRequest()
                };
            }
        }
    }
}
