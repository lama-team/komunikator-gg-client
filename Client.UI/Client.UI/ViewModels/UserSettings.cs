﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.ViewModels
{
    public class UserSettings : BaseViewModel
    {
        public UserSettings()
        {
            ShowProfileSettings = true;
        }

        private bool _alternativeCloseMinimalizeButtons;
        public bool AlternativeCloseMinimalizeButtons
        {
            get { return _alternativeCloseMinimalizeButtons; }
            set { _alternativeCloseMinimalizeButtons = value; OnPropertyChanged("AlternativeCloseMinimalizeButtons"); }
        }

        private bool _showProfileSettings;
        public bool ShowProfileSettings
        {
            get { return _showProfileSettings; }
            set
            {
                _showProfileSettings = value;
                if (value)
                    ShowSettingsSettings = false;
                OnPropertyChanged("ShowProfileSettings");
            }
        }

        private bool _showSettingsSettings;
        public bool ShowSettingsSettings
        {
            get { return _showSettingsSettings; }
            set
            {
                _showSettingsSettings = value;
                if (value)
                    ShowProfileSettings = false;
                OnPropertyChanged("ShowSettingsSettings");
            }
        }

    }
}
