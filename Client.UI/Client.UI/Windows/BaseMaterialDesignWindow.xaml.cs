﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Autofac;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for BaseMaterialDesignWindow.xaml
    /// </summary>
    public partial class BaseMaterialDesignWindow : Window {
        public bool Shutdown { get; set; } = false;
        public BaseMaterialDesignWindow()
        {
            InitializeComponent();
        }

        public void SetContent(UserControl content)
        {
            Content.Content = content;
        }
        protected override void OnClosed(EventArgs e) {
            if (Shutdown) {
                Container.OnExit();
            }
        }
    }
}
