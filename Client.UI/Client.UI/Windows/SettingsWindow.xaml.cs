﻿using Autofac;
using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        private User _viewModel;
        public SettingsWindow()
        {
            InitializeComponent();
            this.DataContext = _viewModel = Container.Instance.Resolve<User>();
            Header.MouseDown += Header_MouseDown;
        }

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                base.DragMove();
        }

        public static void ShowSettingsDialog()
        {
            var wind = new SettingsWindow();
            wind.ShowDialog();
        }

        private void SaveAndExit_Click(object sender, RoutedEventArgs e)
        {
            Container.Instance.Resolve<User>().UpdateData();
            this.Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SettingsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SettingsContent.Content = new SettingsSettingsContent { DataContext = _viewModel };
        }

        private void ProfileRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SettingsContent.Content = new ProfileSettingsContent { DataContext = _viewModel };
        }
    }
}
