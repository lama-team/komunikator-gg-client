﻿using Autofac;
using Client.UI.IoC;
using Client.UI.Singletons;
using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Contacts : Window
    {
        public Contacts()
        {
            InitializeComponent();
            Header.MouseDown += Header_MouseDown;
        }

        private void HideAllApplicationWindows()
        {
            this.Hide();
            if (Container.Instance.Resolve<ConversationWindowTracker>().IsResolved)
                Container.Instance.Resolve<ConversationWindow>().Hide();
        }

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                base.DragMove();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            if (Container.Instance.Resolve<User>().Settings.AlternativeCloseMinimalizeButtons)
                Application.Current.Shutdown();
            else
                HideAllApplicationWindows();
        }

        private void Minimalize_Click(object sender, RoutedEventArgs e)
        {
            if (Container.Instance.Resolve<User>().Settings.AlternativeCloseMinimalizeButtons)
                HideAllApplicationWindows();
            else
                this.WindowState = WindowState.Minimized;
        }
    }
}
