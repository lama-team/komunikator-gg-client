﻿using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for FindNewContact.xaml
    /// </summary>
    public partial class FindNewContact : Window
    {
        public FindNewContact(FindNewContactViewModel viewModel)
        {
            InitializeComponent();
            viewModel.SetWindow(this);
            this.DataContext = viewModel;
            Header.MouseDown += Header_MouseDown;
        }

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                base.DragMove();
        }

        private void Close_click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
