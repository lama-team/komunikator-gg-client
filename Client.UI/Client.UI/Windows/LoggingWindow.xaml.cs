﻿using Client.UI.ViewModels;
using System.Windows;
using Autofac;
using System.Windows.Controls;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for LoggingWindow.xaml
    /// </summary>
    public partial class LoggingWindow : UserControl
    {
        private static BaseMaterialDesignWindow _window;
        public LoggingWindow(LoggingViewModel viewModel)
        {
            InitializeComponent();
            _window = new BaseMaterialDesignWindow();
            _window.SetContent(this);
            DataContext = viewModel;
            Loaded += new RoutedEventHandler((s, e) => UsernameTextbox.Focus());
            viewModel.Window = this;
        } 

        public BaseMaterialDesignWindow Window
        {
            get
            {
                return _window;
            }
        }

        public void ShowWindow()
        {
            _window.ShowDialog();
        }

        public void CloseWindow()
        {
            _window.Close();
        }

        private void Exit(object sender, RoutedEventArgs e) {
            //_window.Shutdown = true;
            //_window.Close();
            Application.Current.Shutdown();
        }

        private void Register(object sender, RoutedEventArgs e)
        {
            Container.Instance.Resolve<RegisterWindow>().ShowWindow();
        }
    }
}
