﻿using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.UI.Windows
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : UserControl
    {
        private static BaseMaterialDesignWindow _window;
        public RegisterWindow(RegisterViewModel viewModel)
        {
            InitializeComponent();
            _window = new BaseMaterialDesignWindow();
            _window.SetContent(this);
            DataContext = viewModel;
            viewModel.SetWindow(this);
        }

        public void ShowWindow()
        {
            _window.ShowDialog();
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            _window.Close();
        }

        public void CloseWindow()
        {
            _window.Close();
        }
    }
}
