﻿using Autofac;
using Client.UI.IoC;
using Client.UI.Windows;
using System;
using System.Windows;
using System.Windows.Input;
using static Client.UI.ViewModels.BaseViewModel;

namespace Client.UI
{
    public class NotifyIconViewModel
    {
        public ICommand ShowWindowCommand
        {
            get
            {
                return new DelegateCommand
                {
                    CanExecuteFunc = () => Container.Instance.Resolve<LoggedInTracker>().IsLogged, // Application.Current.MainWindow == null,
                    CommandAction = () =>
                    {
                        Container.Instance.Resolve<Contacts>().Show();
                        //Application.Current.MainWindow = new MainWindow();
                        //Application.Current.MainWindow.Show();
                    }
                };
            }
        }
        
        //public ICommand HideWindowCommand
        //{
        //    get
        //    {
        //        return new DelegateCommand
        //        {
        //            CanExecuteFunc = () => Application.Current.MainWindow != null,
        //            CommandAction = () => Application.Current.MainWindow.Close()
        //        };
        //    }
        //}
        
        public ICommand ExitApplicationCommand
        {
            get
            {
                return new DelegateCommand {CommandAction = () => Application.Current.Shutdown()};
            }
        }
    }
}
