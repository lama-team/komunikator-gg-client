﻿using Autofac;
using Client.Service.Model.Incoming.User;
using Client.UI.IoC;
using Client.UI.ViewModels;
using Client.UI.Windows;
using MediatR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;

namespace Client.UI.Handlers
{
    public class GetStateHandlers
        : INotificationHandler<GetUserStateSuccessNotification>
        , INotificationHandler<GetUserStateErrorNotification>
    {
        public void Handle(GetUserStateErrorNotification notification)
        {
            //log error;
        }

        private void HandleMessages(GetUserStateSuccessNotification notification)
        {
            ConversationsViewModel viewModel = Container.Instance.Resolve<ConversationsViewModel>();
            if (viewModel.DisplayedConversation != null)
            {
                var messages = new ObservableCollection<Message>();
                foreach (var groups in notification.Contacts.First(q => q.Contact.UserId == viewModel.DisplayedConversation.Name).Messages
                    .OrderBy(Timestamp)
                    .GroupBy(TimestampHash))

                foreach (var item in groups) {

                        messages.Add(
                        new Message {
                            Content =
                                $"{GetTimestamp(item,groups)} {item.Content}",
                            MessageSource = item.SourceUserId == viewModel.DisplayedConversation.Name
                                ? Message.MessageSourceEnum.From
                                : Message.MessageSourceEnum.To
                        });
                }

                Container.Instance.Resolve<ConversationWindow>().Dispatcher.BeginInvoke(
                System.Windows.Threading.DispatcherPriority.ContextIdle,
                new Action(delegate ()
                {
                    if (viewModel?.DisplayedConversation?.Messages != null) {
                        viewModel.DisplayedConversation.Messages = messages;
                    }
                }));
            }
        }

        private string GetTimestamp(Contract.Message.Message item, IGrouping<string, Contract.Message.Message> groups) {
            if (item != null && groups.FirstOrDefault() == item) {
                return Guid.Parse(item.MessageId).ToNewId().Timestamp.ToShortTimeString();
            }
            return String.Empty;

        }

        private static string TimestampHash(Contract.Message.Message q) {
            return $"{Timestamp(q).Year}{Timestamp(q).Month}{Timestamp(q).Day}{Timestamp(q).Hour}{Timestamp(q).Minute}";
        }

        private static DateTime Timestamp(Contract.Message.Message q) {
            return Guid.Parse(q.MessageId).ToNewId().Timestamp;
        }

        private void HandleContacts(GetUserStateSuccessNotification notification)
        {
            ContactsViewModel viewModel = Container.Instance.Resolve<ContactsViewModel>();

            var invitations = new ObservableCollection<PendingContact>();
            var contacts = new ObservableCollection<Contact>();

            foreach (var item in notification.ContactRequests)
                invitations.Add(new PendingContact(viewModel) { Name = item.SourceUserId });
            foreach (var item in notification.Contacts)
                contacts.Add(new Contact(viewModel) { Name = item.Contact.UserName, Description = item.Contact.Description});

            Container.Instance.Resolve<Contacts>().Dispatcher.BeginInvoke(
            System.Windows.Threading.DispatcherPriority.ContextIdle,
            new Action(delegate ()
            {
                viewModel.Invitations = invitations;
                viewModel.Contacts = contacts;
            }));
        }

        public void Handle(GetUserStateSuccessNotification notification)
        {
            HandleContacts(notification);
            if (Container.Instance.Resolve<ConversationWindowTracker>().IsResolved)
                HandleMessages(notification);
        }
    }
}
