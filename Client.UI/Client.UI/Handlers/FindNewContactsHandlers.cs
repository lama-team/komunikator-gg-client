﻿using Client.Service.Model.Incoming.Contact;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client.UI.Handlers
{
    public class FindNewContactsHandlers
         : INotificationHandler<ContactRequestSuccessNotification>
        , INotificationHandler<ContactRequestErrorNotification>
    {
        public void Handle(ContactRequestSuccessNotification notification)
        {
            MessageBox.Show("Wysłano zaproszenie do kontaktów pomyślnie.");
        }

        public void Handle(ContactRequestErrorNotification notification)
        {
            MessageBox.Show(notification.Exception.Message);
        }
    }
}
