﻿using Autofac;
using Client.UI.IoC;
using Client.UI.ViewModels;
using Client.UI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client.UI.Singletons
{
    public static class ContactsWindowManager
    {
        private static Contacts _window;
        private static ContactsViewModel _viewModel;

        public static void Logged(User user)
        {
            _viewModel = Container.Instance.Resolve<ContactsViewModel>(); //new ContactsViewModel(user);
            _window = Container.Instance.Resolve<Contacts>();
            Container.Instance.Resolve<LoggedInTracker>().IsLogged = true;
            _window.Dispatcher.Invoke((Action)(() =>
            {
                _window.DataContext = _viewModel;
                _window.Show();
            }));
        }

        public static void ShowWindow()
        {
            if (_window != null)
                _window.Show();
        }
    }
}
