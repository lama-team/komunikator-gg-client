﻿using Autofac;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Service;
using System.Reflection;
using MediatR;
using System.Threading;
using System.Windows;

namespace Client.UI
{
    public class Container
    {
        private static IContainer _container;
        private static IClient _client;
        private static Thread _thread;
        private static ILifetimeScope _scope;
        private Container() { }

        public static IContainer Instance
        {
            get
            {
                if (_container == null)
                {
                    var builder = new ContainerBuilder();
                    builder.RegisterModule<Service.IoC.Module>();
                    builder.RegisterModule<IoC.Module>();
                    //builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                    //    .AsClosedTypesOf(typeof(INotificationHandler<>)).AsImplementedInterfaces(); // via assembly scan
                    //builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                    //    .AsClosedTypesOf(typeof(IAsyncNotificationHandler<>)).AsImplementedInterfaces();
                    _container = builder.Build();
                    //_client = _container.Resolve<IClient>();
                    //_thread = _client.StartOnNewThread();
                    _scope = _container.BeginLifetimeScope();
                }
                return _container;
            }
        }

        public static IMediator Mediator
        {
            get { return Instance.Resolve<IMediator>(); }
        }

        public static void OnExit()
        {
            if (_scope != null)
                _scope.Dispose();
            Environment.Exit(0);
        }
    }
}
