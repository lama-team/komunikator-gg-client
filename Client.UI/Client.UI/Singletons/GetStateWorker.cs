﻿using Client.Service.Model.Outgoing.User;
using Client.UI.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client.UI.Singletons
{
    public class GetStateWorker
    {
        private User _user;
        private static GetStateWorker _instance;

        public GetStateWorker(User user)
        {
            _user = user;
            _getStateBW = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
            };
            _getStateBW.DoWork += GetStateBW_DoWork;
        }

        public static void StartWorking(User user)
        {
            _instance = new GetStateWorker(user);
            _instance.RunGetStateWorker();
        }

        public static void StopWorker()
        {
            if (_instance != null)
                _instance.CancelGetStateWorker();
        }

        BackgroundWorker _getStateBW;

        private void GetStateBW_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!((BackgroundWorker)sender).CancellationPending)
            {
                //Dispatcher.CurrentDispatcher.BeginInvoke(
                //DispatcherPriority.ContextIdle,
                //new Action(delegate ()
                //{
                Container.Mediator.Send(new GetUserStateRequest
                {
                    CorrelationId = Guid.NewGuid(),
                    Token = _user.Token
                }).Wait(TimeSpan.FromSeconds(5));
                //}));
                Thread.Sleep(800);
            }
        }

        private void RunGetStateWorker()
        {
            _getStateBW.RunWorkerAsync();
        }

        private void CancelGetStateWorker()
        {
            _getStateBW.CancelAsync();
        }
    }
}
