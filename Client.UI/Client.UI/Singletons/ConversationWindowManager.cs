﻿using Autofac;
using Client.UI.IoC;
using Client.UI.ViewModels;
using Client.UI.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.UI.Singletons
{
    public static class ConversationWindowManager
    {
        private static ConversationsViewModel _viewModel;
        private static ConversationWindow _window;

        public static void OpenConversation(Conversation conversation)
        {
            if (_window == null)
            {
                _window = Container.Instance.Resolve<ConversationWindow>();
                Container.Instance.Resolve<ConversationWindowTracker>().IsResolved = true;
            }
            if (_viewModel == null)
                _viewModel = Container.Instance.Resolve<ConversationsViewModel>(); //new ConversationsViewModel();
            _window.DataContext = _viewModel;
            if (!_viewModel.Conversations.Any(q => q.Name == conversation.Name))
                _viewModel.Conversations.Add(conversation);
            _viewModel.DisplayedConversation = conversation;
            _window.Show();
        }
    }
}
