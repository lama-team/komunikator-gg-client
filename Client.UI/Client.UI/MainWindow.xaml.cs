﻿using System;
using System.Windows;

namespace Client.UI {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }
        protected override void OnClosed(EventArgs e)
        {
            Container.OnExit();
        }
    }
}
