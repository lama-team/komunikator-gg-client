﻿using System;
using Client.Contract.Countries;

namespace Client.Service.Model.Outgoing.Countries
{
    public class GetCountriesRequest : IGetCountriesRequest
    {
        public Guid CorrelationId { get; set; }
    }
}
