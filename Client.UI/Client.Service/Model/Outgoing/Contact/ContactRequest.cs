﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;

namespace Client.Service.Model.Outgoing.Contact
{
    public class ContactRequest : IContactRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
        public string TargetUserId { get; set; }
    }
}
