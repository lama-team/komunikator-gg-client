﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;

namespace Client.Service.Model.Outgoing.Contact
{
    public class GetContactsRequest : IGetContactsRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public uint? MinAge { get; set; }
        public uint? MaxAge { get; set; }
        public string Gender { get; set; }
    }
}
