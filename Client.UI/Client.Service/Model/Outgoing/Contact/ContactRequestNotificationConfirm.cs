﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;

namespace Client.Service.Model.Outgoing.Contact
{
    public class ContactRequestNotificationConfirm : IContactRequestNotificationConfirm
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
        public string SourceUserId { get; set; }
        public bool Confirm { get; set; }
    }
}
