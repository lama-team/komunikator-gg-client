﻿using System;
using MediatR;

namespace Client.Service.Model.Outgoing
{
    public class BaseSendMessage : IRequest {
        public byte[] Data { get; set; }
        public Guid CorrelationId { get; set; }
        public string MessageType { get; set; }
        public string Token { get; set; }

    }

    public class BaseReceiveMessage : INotification
    {
        public byte[] Data { get; set; }
        public string MessageType { get; set; }
    }
}
