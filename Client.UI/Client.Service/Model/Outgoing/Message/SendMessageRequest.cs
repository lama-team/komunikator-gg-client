﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Message;

namespace Client.Service.Model.Outgoing.Message
{
    public class SendMessageRequest : ISendMessageRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
        public string TargetUserId { get; set; }
        public Guid MessageId { get; set; }
        public string Content { get; set; }
    }
}
