﻿using System;
using Client.Contract.Echo;

namespace Client.Service.Model.Outgoing.Echo {
    public class EchoRequest : IEchoRequest{
        public Guid CorrelationId { get; set; }
        public string Message { get; set; }
    }
}
