﻿using System;
using Client.Contract.Login;

namespace Client.Service.Model.Outgoing.Login {
    public class LoginRequest : ILoginRequest{
        public Guid CorrelationId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
