﻿using System;
using Client.Contract.Register;

namespace Client.Service.Model.Outgoing.Register {
    public class RegisterRequest : IRegisterRequest{
        public Guid CorrelationId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
