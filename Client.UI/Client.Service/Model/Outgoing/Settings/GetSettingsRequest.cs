﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Settings;

namespace Client.Service.Model.Outgoing.Settings
{
    public class GetSettingsRequest : IGetSettingsRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
    }
}
