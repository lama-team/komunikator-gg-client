﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.User;

namespace Client.Service.Model.Outgoing.User
{
    public class GetUserStateRequest : IGetUserStateRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
    }
}
