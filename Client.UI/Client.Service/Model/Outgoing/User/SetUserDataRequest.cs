﻿using System;
using Client.Contract.User;

namespace Client.Service.Model.Outgoing.User
{
    public class SetUserDataRequest : ISetUserDataRequest
    {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
        public Contract.Contact.Contact Contact { get; set; }
    }
}
