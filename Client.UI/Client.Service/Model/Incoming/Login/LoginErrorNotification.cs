﻿using System;
using Client.Contract.Core;
using Client.Contract.Login;

namespace Client.Service.Model.Incoming.Login {
    public class LoginErrorNotification : ILoginErrorNotification{
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
