﻿using System;
using Client.Contract.Login;

namespace Client.Service.Model.Incoming.Login {
    public class LoginSuccessNotification : ILoginSuccessNotification {
        public Guid CorrelationId { get; set; }
        public string Token { get; set; }
    }
}
