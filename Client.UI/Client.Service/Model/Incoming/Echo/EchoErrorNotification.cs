﻿using System;
using Client.Contract.Core;
using Client.Contract.Echo;

namespace Client.Service.Model.Incoming.Echo {
    public class EchoErrorNotification : IEchoErrorNotification{
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
