﻿using System;
using Client.Contract.Echo;

namespace Client.Service.Model.Incoming.Echo {
    public class EchoSuccessNotification : IEchoSuccessNotification{
        public Guid CorrelationId { get; set; }
        public string Message { get; set; }
    }
}
