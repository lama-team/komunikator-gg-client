﻿using System;
using Client.Contract.Register;

namespace Client.Service.Model.Incoming.Register {
    public class RegisterSuccessNotification : IRegisterSuccessNotification {
        public Guid CorrelationId { get; set; }
    }
}
