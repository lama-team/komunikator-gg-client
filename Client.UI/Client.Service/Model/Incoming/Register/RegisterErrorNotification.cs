﻿using System;
using Client.Contract.Core;
using Client.Contract.Register;

namespace Client.Service.Model.Incoming.Register {
    public class RegisterErrorNotification : IRegisterErrorNotification {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
