﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Contract.Core;

namespace Client.Service.Model.Incoming.Contact
{
    public class ContactRequestNotificationConfirmErrorNotification : IContactRequestNotificationConfirmErrorNotification
    {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
