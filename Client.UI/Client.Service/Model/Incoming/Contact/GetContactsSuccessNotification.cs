﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;

namespace Client.Service.Model.Incoming.Contact
{
    public class GetContactsSuccessNotification : IGetContactsSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public IEnumerable<Contract.Contact.Contact> Contacts { get; set; }
    }
}
