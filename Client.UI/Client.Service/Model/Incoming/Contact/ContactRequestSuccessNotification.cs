﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;

namespace Client.Service.Model.Incoming.Contact
{
    public class ContactRequestSuccessNotification : IContactRequestSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public string SourceUserId { get; set; }
        public string TargetUserId { get; set; }
        public bool Accepted { get; set; }
    }
}
