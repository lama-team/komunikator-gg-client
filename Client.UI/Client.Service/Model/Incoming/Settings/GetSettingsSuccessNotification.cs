﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Settings;

namespace Client.Service.Model.Incoming.Settings
{
    public class GetSettingsSuccessNotification : IGetSettingsSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public Dictionary<string, string> Settings { get; set; }
    }
}
