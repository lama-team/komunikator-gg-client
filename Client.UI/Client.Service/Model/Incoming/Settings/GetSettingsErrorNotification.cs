﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;
using Client.Contract.Settings;

namespace Client.Service.Model.Incoming.Settings
{
    public class GetSettingsErrorNotification : IGetSettingsErrorNotification
    {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
