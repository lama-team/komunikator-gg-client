﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.User;

namespace Client.Service.Model.Incoming.User
{
    public class SetUserDataSuccessNotification : ISetUserDataSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public Contract.Contact.Contact Contact { get; set; }
    }
}
