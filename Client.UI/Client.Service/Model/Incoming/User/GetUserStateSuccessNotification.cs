﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Contract.User;

namespace Client.Service.Model.Incoming.User
{
    public class GetUserStateSuccessNotification : IGetUserStateSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public Contract.Contact.Contact User { get; set; }
        public IEnumerable<ContactRequest> ContactRequests { get; set; }
        public IEnumerable<ContactWithMessages> Contacts { get; set; }
    }
}
