﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Core;
using Client.Contract.Message;

namespace Client.Service.Model.Incoming.Message
{
   public class SendMessageErrorNotification : ISendMessageErrorNotification
    {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
