﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Message;

namespace Client.Service.Model.Incoming.Message
{
    public class SendMessageSuccessNotification : ISendMessageSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public string TargetUserId { get; set; }
        public string MessageId { get; set; }
        public string Content { get; set; }
    }
}
