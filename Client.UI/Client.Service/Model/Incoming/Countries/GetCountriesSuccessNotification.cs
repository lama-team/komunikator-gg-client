﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract.Countries;

namespace Client.Service.Model.Incoming.Countries
{
    public class GetCountriesSuccessNotification : IGetCountriesSuccessNotification
    {
        public Guid CorrelationId { get; set; }
        public IEnumerable<string> Countries { get; set; }
    }
}
