﻿using System;
using Client.Contract.Core;
using Client.Contract.Countries;

namespace Client.Service.Model.Incoming.Countries
{
    public class GetCountriesErrorNotification : IGetCountriesErrorNotification
    {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
