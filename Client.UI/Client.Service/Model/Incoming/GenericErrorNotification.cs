﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.Contract;
using Client.Contract.Core;

namespace Client.Service.Model.Incoming
{
    public class GenericErrorNotification : IGenericErrorNotification
    {
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
