﻿using System;
using Client.Contract.Core;
using Client.Contract.Core.Notification;

namespace Client.Service.Model.Incoming {
    public class ExceptionNotification : IErrorNotification{
        public Guid CorrelationId { get; set; }
        public MessageStatus Status { get; set; }
        public Exception Exception { get; set; }
    }
}
