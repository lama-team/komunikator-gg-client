﻿using System.Collections.Generic;
using Client.Contract.Core;

namespace Client.Service.Model.Incoming {
    public class GenericResponse {
        public string Path { get; set; }
        public string Method { get; set; }
        public Dictionary<string,string> Headers { get; set; }
        public object Body { get; set; }
        public MessageStatus Status { get; set; }
    }
}
