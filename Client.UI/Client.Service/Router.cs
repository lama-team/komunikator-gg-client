﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Client.Contract.Core;
using Client.Contract.Core.Notification;
using Client.Service.Model.Incoming;
using Client.Service.Model.Outgoing;
using MediatR;

namespace Client.Service {
    public class Router : IAsyncNotificationHandler<BaseReceiveMessage>
    {
        private readonly IRouteMap routeMap;
        private readonly IMediator mediator;
        private static readonly Dictionary<Guid, DateTimeOffset> Messages = new Dictionary<Guid, DateTimeOffset>();
        private static readonly SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(1, 1);
        public Router(IRouteMap routeMap, IMediator mediator) {
            this.routeMap = routeMap;
            this.mediator = mediator;
        }

        public async Task Handle(BaseReceiveMessage message) {
            var option = routeMap.Map(message);
            await option.Match(
                some: async x => {
                    var success = x as ISuccessNotification;
                    var error = x as IErrorNotification;
                    Guid correlationId = Guid.Empty;
                    if (success != null) {
                        correlationId = success.CorrelationId;
                    }
                    if (error != null) {
                        correlationId = error.CorrelationId;
                    }
                    await SemaphoreSlim.WaitAsync(1000);
                    try {
                        if (!Messages.ContainsKey(correlationId)) {
                            Messages.Add(correlationId, DateTimeOffset.Now);
                            await mediator.Publish(x);
                        }
                    }
                    finally {
                        SemaphoreSlim.Release();
                    }
                    if (Messages.Count > 1000) {
                        var now = DateTimeOffset.Now;
                        foreach (var m in Messages.Where(mess => now < mess.Value.AddSeconds(5))) {
                            Messages.Remove(m.Key);
                        }
                    }
                },
                none: async x => {
                    var notification = new ExceptionNotification
                    {
                        Exception = x,
                        Status = MessageStatus.ServiceLayerException
                    };
                    await mediator.Publish(notification);
                });
        }
    }

}
