﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using Contract.Komunikator;
using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using MassTransit;
using Exception = System.Exception;
using System.Linq;
using System.Collections.Generic;
using Client.Service.Model.Outgoing;
using MediatR;

namespace Client.Service {
    

    // State object for receiving data from remote device.  
    public class StateObject {
        // Client socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 4096;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public List<byte> response = new List<byte>(4096);
    }

    public interface IClient : IRequestHandler<BaseSendMessage>
    {
        Thread StartOnNewThread();
        void StartClient();
        void Kill();
    }

    public class AsynchronousClient : IClient
    {
        private readonly IMediator mediator;

        public AsynchronousClient(IMediator mediator) {
            this.mediator = mediator;
        }
        // The port number for the remote device.  
        private const int port = 1025;

        private Socket client = null;

        // ManualResetEvent instances signal completion.  
        private readonly ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private readonly ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private readonly ManualResetEvent receiveDone =
            new ManualResetEvent(false);
        private readonly ManualResetEvent sendMessage = 
            new ManualResetEvent(false);

        public void Kill() {
            if (client != null && client.Connected)
            {
                // Release the socket.  
                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
        }

        private readonly Queue<byte[]> messagesToSend = new Queue<byte[]>();

        public void SendMessage(byte[] sendMessage) {
            messagesToSend.Enqueue(sendMessage);
            this.sendMessage.Set();
        }

        private byte[] GetMessage() {
            if (messagesToSend.Count == 1) {
                sendMessage.Reset();
            }
            return messagesToSend.Dequeue();
        }

        // The response from the remote device.  
        private String responseMessageType = String.Empty;
        private readonly String response = String.Empty;

        public Thread StartOnNewThread() {
            Thread thread = new Thread(StartClient);
            thread.Start();
            return thread;
        }

        public void StartClient() {
            // Connect to a remote device.  
            try {
                // Establish the remote endpoint for the socket.  
                // The name of the   
                // remote device is "host.contoso.com".  
                //IPHostEntry ipHostInfo = Dns.GetHostEntry("host.contoso.com");
                //IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Create a TCP/IP socket.  
                client = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint.  
                client.BeginConnect(remoteEP,
                    new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne(TimeSpan.FromSeconds(10));

                while (true) {
                    sendMessage.WaitOne();
                    sendDone.Reset();
                    receiveDone.Reset();
                    // Send test data to the remote device.  
                    Send(client, GetMessage());
                    try {
                        sendDone.WaitOne(TimeSpan.FromSeconds(10));

                        // Receive the response from the remote device.  
                        Receive(client);
                        receiveDone.WaitOne(TimeSpan.FromSeconds(15));
                    }
                    catch (Exception e) {
                        Console.WriteLine("EXCEPTION: " + e);
                    }

                }

            }
            catch (ThreadAbortException e)
            {
                if (client != null) {
                    // Release the socket.  
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
            if (client != null)
            {
                // Release the socket.  
                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
        }

        private void ConnectCallback(IAsyncResult ar) {
            try {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.  
                client.EndConnect(ar);

                Console.WriteLine("Socket connected to {0}",
                    client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.  
                connectDone.Set();
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }

        private void Receive(Socket client) {
            try {
                // Create the state object.  
                StateObject state = new StateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }

        private void ReceiveCallback(IAsyncResult ar) {
            try {
                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject) ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.  
                int bytesRead = client.EndReceive(ar);

                // There might be more data, so store the data received so far.  
                state.response.AddRange(state.buffer.Take(bytesRead));
                var headerLen = state.response[0];
                var headerArray = state.response.Skip(1).Take(headerLen);
                var header = Encoding.UTF8.GetString(headerArray.ToArray());
                var responseLenArray = state.response.Skip(headerLen + 1).Take(4).Reverse().ToArray();
                var responseLen = BitConverter.ToInt32(responseLenArray, 0);
                var totalLen = headerLen + responseLen + 1 + 4;
                if (totalLen != bytesRead) {
                    // Get the rest of the data.  
                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else {
                    var response = state.response.Skip(headerLen + 5).Take(responseLen).ToArray();
                    mediator.Publish(new BaseReceiveMessage
                    {
                        Data = response,
                        MessageType = header
                    }).Wait(TimeSpan.FromSeconds(5));
                    state.response.Clear();
                    receiveDone.Set();
                }
                if (bytesRead == 0) {
                    state.response.Clear();
                    receiveDone.Set();
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
            finally {
                receiveDone.Set();
            }
        }

        public static byte[] Combine(byte[] first, byte[] second) {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        private void Send(Socket client, byte[] data) {
            client.BeginSend(data, 0, data.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private byte[] CreateMessage(string messageType, Guid correlationId, string token, byte[] data) {
            // Convert the string data to byte data using ASCII encoding. 
            byte[] dataLength = BitConverter.GetBytes(data.Length);
            byte[] tokenLength = BitConverter.GetBytes((short)token.Length);
            byte[] byteData = Combine(
                Combine(
                    Combine(new[] {
                    (byte)messageType.Length},
                    Encoding.UTF8.GetBytes(messageType)),
                    Combine(
                    dataLength,
                    Encoding.UTF8.GetBytes(correlationId.ToString()))
                ),
                Combine(
                    Combine(
                        tokenLength,
                        Encoding.UTF8.GetBytes(token)),
                    data
                )
            );
            return byteData;
        }

        private void SendCallback(IAsyncResult ar) {
            try {
                // Retrieve the socket from the state object.  
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }

        public void Handle(BaseSendMessage message) {
            SendMessage(CreateMessage(message.MessageType, message.CorrelationId, message.Token ?? "0", message.Data));
        }
    }
}
