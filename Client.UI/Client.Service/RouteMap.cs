﻿using Client.Service.Factory;
using Client.Service.Model.Outgoing;
using MediatR;
using Optional;
using Exception = System.Exception;

namespace Client.Service {
    public interface IRouteMap {
        Option<INotification, Exception> Map(BaseReceiveMessage message);
    }
    public class RouteMap : IRouteMap {
        private readonly HandlerFactory factory;

        public RouteMap(HandlerFactory factory) {
            this.factory = factory;
        }

        public Option<INotification, Exception> Map(BaseReceiveMessage message) {
            try {
                return Option.Some<INotification, Exception>(factory.Get(message));
            }
            catch (Exception e) {
                return Option.None<INotification, Exception>(e);
            }
        }

    }
}
