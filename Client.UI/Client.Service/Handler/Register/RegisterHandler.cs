﻿using System;
using System.Threading.Tasks;
using Client.Contract.Register;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Register;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Register {
    public class RegisterHandler : BaseHandler<IRegisterRequest, RegisterSuccessResponse, RegisterSuccessNotification,
        RegisterErrorResponse, RegisterErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public RegisterHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IRegisterRequest message) {
            RegisterSuccessResponse response = null;
            try {
                var req = new RegisterRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Password = message.Password,
                    Username = message.Login
                };
                response = client.Register(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new RegisterErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override RegisterSuccessNotification HandleSuccessNotification(RegisterSuccessResponse message) {
            return new RegisterSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId)
            };
        }

        public override RegisterErrorNotification HandleErrorNotification(RegisterErrorResponse message) {
            return new RegisterErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }
    }
}