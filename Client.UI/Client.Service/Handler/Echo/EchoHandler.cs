﻿using System;
using System.Threading.Tasks;
using Client.Contract.Echo;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Echo;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Echo {
    public class EchoHandler : BaseHandler<IEchoRequest, EchoSuccessResponse, EchoSuccessNotification, EchoErrorResponse
        , EchoErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public EchoHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IEchoRequest message) {
            EchoSuccessResponse response = null;
            try {
                var req = new EchoRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Message = message.Message
                };
                response = client.Echo(req);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid.TryParse(response?.CorrelationId, out var g);
                await mediator.Publish(new EchoErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override EchoSuccessNotification HandleSuccessNotification(EchoSuccessResponse message) {
            return new EchoSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Message = message.Message
            };
        }

        public override EchoErrorNotification HandleErrorNotification(EchoErrorResponse message) {
            return new EchoErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }
    }
}