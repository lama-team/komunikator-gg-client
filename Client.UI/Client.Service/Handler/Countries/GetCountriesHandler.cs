﻿using System;
using System.Threading.Tasks;
using Client.Contract.Countries;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Countries;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Countries {
    public class GetCountriesHandler : BaseHandler<IGetCountriesRequest, GetCountriesSuccessResponse,
        GetCountriesSuccessNotification, GetCountriesErrorResponse, GetCountriesErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public GetCountriesHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IGetCountriesRequest message) {
            GetCountriesSuccessResponse response = null;
            try {
                var req = new GetCountriesRequest {
                    CorrelationId = message.CorrelationId.ToString()
                };
                response = client.GetCountries(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid.TryParse(response?.CorrelationId, out var g);
                await mediator.Publish(new GetCountriesErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override GetCountriesErrorNotification HandleErrorNotification(GetCountriesErrorResponse message) {
            return new GetCountriesErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override GetCountriesSuccessNotification HandleSuccessNotification(GetCountriesSuccessResponse message) {
            return new GetCountriesSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Countries = message.Countries
            };
        }
    }
}