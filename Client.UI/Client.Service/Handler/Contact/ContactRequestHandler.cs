﻿using System;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Contact;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Contact {
    public class ContactRequestHandler : BaseHandler<IContactRequest, ContactRequestSuccessResponse,
        ContactRequestSuccessNotification, ContactRequestErrorResponse, ContactRequestErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public ContactRequestHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IContactRequest message) {
            ContactRequestSuccessResponse response = null;
            try {
                var req = new ContactRequestRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    TargetUserId = message.TargetUserId,
                    Token = message.Token
                };
                response = client.CreateContactRequest(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new ContactRequestErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override ContactRequestErrorNotification HandleErrorNotification(ContactRequestErrorResponse message) {
            return new ContactRequestErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override ContactRequestSuccessNotification HandleSuccessNotification(
            ContactRequestSuccessResponse message) {
            return new ContactRequestSuccessNotification {
                Accepted = message?.ContactRequest?.Accepted ?? false,
                CorrelationId = Guid.Parse(message.CorrelationId),
                SourceUserId = message?.ContactRequest?.SourceUserId,
                TargetUserId = message?.ContactRequest?.TargetUserId
            };
        }
    }
}