﻿using System;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Service.Model.Incoming.Contact;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Contact {
    public class ContactRequestNotificationConfirmHandler : BaseHandler<IContactRequestNotificationConfirm,
        ContactRequestNotificationConfirmSuccessResponse, ContactRequestNotificationConfirmSuccessNotification,
        ContactRequestNotificationConfirmErrorResponse, ContactRequestNotificationConfirmErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public ContactRequestNotificationConfirmHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IContactRequestNotificationConfirm message) {
            ContactRequestNotificationConfirmSuccessResponse response = null;
            try {
                var req = new ContactRequestNotificationConfirm {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token,
                    Confirm = message.Confirm,
                    SourceUserId = message.SourceUserId
                };
                response = client.CreateContactRequestNotificationConfirm(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new ContactRequestNotificationConfirmErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override ContactRequestNotificationConfirmErrorNotification HandleErrorNotification(
            ContactRequestNotificationConfirmErrorResponse message) {
            return new ContactRequestNotificationConfirmErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId)
            };
        }

        public override ContactRequestNotificationConfirmSuccessNotification HandleSuccessNotification(
            ContactRequestNotificationConfirmSuccessResponse message) {
            return new ContactRequestNotificationConfirmSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId)
            };
        }
    }
}