﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Client.Contract.Contact;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Contact;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Contact {
    public class GetContactsHandler : BaseHandler<IGetContactsRequest, GetContactsSuccessResponse,
        GetContactsSuccessNotification, GetContactsErrorResponse, GetContactsErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public GetContactsHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IGetContactsRequest message) {
            GetContactsSuccessResponse response = null;
            try {
                var req = new GetContactsRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token,
                    City = message.City ?? "",
                    Country = message.Country ?? "",
                    FirstName = message.FirstName ?? "",
                    Gender = message.Gender ?? "",
                    LastName = message.LastName ?? "",
                    MaxAge = message.MaxAge ?? 0,
                    MinAge = message.MinAge ?? 0,
                    Username = message.UserName ?? ""
                };
                response = client.GetContacts(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid.TryParse(response?.CorrelationId, out var g);
                await mediator.Publish(new GetContactsErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override GetContactsErrorNotification HandleErrorNotification(GetContactsErrorResponse message) {
            return new GetContactsErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override GetContactsSuccessNotification HandleSuccessNotification(GetContactsSuccessResponse message) {
            return new GetContactsSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Contacts = message.Contacts.Select(x => new Contract.Contact.Contact {
                    Age = x.Age,
                    Avatar = x.Avatar?.Image?.Content?.ToByteArray(),
                    City = x.City,
                    Country = x.Country,
                    Description = x.Description,
                    FirstName = x.FirstName,
                    Gender = x.Gender,
                    LastName = x.LastName,
                    Status = x.Status,
                    UserName = x.Username,
                    UserId = x.UserId
                })
            };
        }
    }
}