﻿using System;
using System.Threading.Tasks;
using Client.Contract.Login;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Login;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Login {
    public class LoginHandler : BaseHandler<ILoginRequest, LoginSuccessResponse, LoginSuccessNotification,
        LoginErrorResponse, LoginErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public LoginHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(ILoginRequest message) {
            LoginSuccessResponse response = null;
            try {
                var request = new LoginRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Login = message.Login,
                    Password = message.Password
                };
                response = client.Login(request);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new LoginErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }


        public override LoginSuccessNotification HandleSuccessNotification(LoginSuccessResponse message) {
            return new LoginSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Token = message.Token
            };
        }

        public override LoginErrorNotification HandleErrorNotification(LoginErrorResponse message) {
            return new LoginErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }
    }
}