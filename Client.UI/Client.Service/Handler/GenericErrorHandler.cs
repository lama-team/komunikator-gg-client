﻿using System;
using Client.Service.Helpers;
using Client.Service.Model.Incoming;
using Contract.Komunikator;
using Exception = System.Exception;

namespace Client.Service.Handler {
    public class GenericErrorHandler : IErrorMap<GenericError, GenericErrorNotification> {
        public GenericError HandleError(byte[] message) {
            return GenericError.Parser.ParseFrom(message);
        }

        public GenericErrorNotification HandleErrorNotification(GenericError message) {
            return new GenericErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(
                    $"{message.CorrelationId}-{message.Created}-{message.Exception?.Message}-{message.Exception?.Type}-{message.MessageType}-{message.Status}"),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }
    }
}