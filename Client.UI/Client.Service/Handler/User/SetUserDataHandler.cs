﻿using System;
using System.Threading.Tasks;
using Client.Contract.User;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.User;
using Contract.Komunikator;
using Google.Protobuf;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.User {
    public class SetUserDataHandler : BaseHandler<ISetUserDataRequest, SetUserDataSuccessResponse,
        SetUserDataSuccessNotification, SetUserDataErrorResponse, SetUserDataErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public SetUserDataHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(ISetUserDataRequest message) {
            SetUserDataSuccessResponse response = null;
            try {
                Avatar avatar;
                var req = new SetUserDataRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token,
                    User = new global::Contract.Komunikator.User {
                        Age = message?.Contact?.Age ?? 0,
                        City = message?.Contact?.City ?? "",
                        Country = message?.Contact?.Country ?? "",
                        Description = message?.Contact?.Description ?? "",
                        FirstName = message?.Contact?.FirstName ?? "",
                        Gender = message?.Contact?.Gender ?? "",
                        LastName = message?.Contact?.LastName ?? "",
                        Status = message?.Contact?.Status ?? "",
                        UserId = message?.Contact?.UserId ?? ""
                    }
                };
                if (message?.Contact?.Avatar?.Length > 0) {
                    req.User.Avatar = new Avatar {
                        Image = new Image {
                            Content = ByteString.CopyFrom(message.Contact.Avatar)
                        }
                    };
                }
                response = client.SetUserData(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new SetUserDataErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override SetUserDataErrorNotification HandleErrorNotification(SetUserDataErrorResponse message) {
            return new SetUserDataErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override SetUserDataSuccessNotification HandleSuccessNotification(SetUserDataSuccessResponse message) {
            return new SetUserDataSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Contact = new Contract.Contact.Contact {
                    Age = message?.User?.Age ?? 0,
                    Avatar = message?.User?.Avatar?.Image?.Content?.ToByteArray(),
                    City = message?.User?.City,
                    Country = message?.User?.Country,
                    Description = message?.User?.Description,
                    FirstName = message?.User?.FirstName,
                    Gender = message?.User?.Gender,
                    LastName = message?.User?.LastName,
                    Status = message?.User?.Status,
                    UserName = message?.User?.Username,
                    UserId = message?.User?.UserId
                }
            };
        }
    }
}