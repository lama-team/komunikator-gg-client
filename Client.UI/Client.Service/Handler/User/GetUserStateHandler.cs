﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Client.Contract.User;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.User;
using Contract.Komunikator;
using MediatR;
using ContactRequest = Client.Contract.Contact.ContactRequest;
using ContactWithMessages = Client.Contract.Contact.ContactWithMessages;
using Exception = System.Exception;

namespace Client.Service.Handler.User {
    public class GetUserStateHandler : BaseHandler<IGetUserStateRequest, GetUserStateSuccessResponse,
        GetUserStateSuccessNotification, GetUserStateErrorResponse, GetUserStateErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public GetUserStateHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IGetUserStateRequest message) {
            GetUserStateSuccessResponse response = null;
            try {
                var req = new GetUserStateRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token
                };
                response = client.GetUserState(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new GetUserStateErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override GetUserStateErrorNotification HandleErrorNotification(GetUserStateErrorResponse message) {
            return new GetUserStateErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override GetUserStateSuccessNotification HandleSuccessNotification(GetUserStateSuccessResponse message) {
            return new GetUserStateSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                User = new Contract.Contact.Contact {
                    City = message.User.City ?? "",
                    Country = message.User.Country ?? "",
                    FirstName = message.User.FirstName ?? "",
                    Gender = message.User.Gender ?? "",
                    LastName = message.User.LastName ?? "",
                    Age = message.User.Age,
                    UserId = message.User.UserId,
                    UserName = message.User.Username,
                    Avatar = message.User.Avatar?.Image?.Content?.ToByteArray()
                },
                Contacts = message.Contacts.Select(x => new ContactWithMessages {
                    Messages = x.Messages.Select(y => new Contract.Message.Message {
                        Content = y.Content,
                        MessageId = y.MessageId,
                        Read = y.Read,
                        SourceUserId = y.SourceUserId,
                        TargetUserId = y.TargetUserId
                    }),
                    Contact = new Contract.Contact.Contact {
                        Age = x.Contact.Age,
                        Avatar = x.Contact.Avatar?.Image?.Content?.ToByteArray(),
                        City = x.Contact.City,
                        Country = x.Contact.Country,
                        Description = x.Contact.Description,
                        FirstName = x.Contact.FirstName,
                        Gender = x.Contact.Gender,
                        LastName = x.Contact.LastName,
                        Status = x.Contact.Status,
                        UserName = x.Contact.Username,
                        UserId = x.Contact.UserId
                    }
                }),
                ContactRequests = message.ContactRequests.Select(x => new ContactRequest {
                    Accepted = x.Accepted,
                    SourceUserId = x.SourceUserId,
                    TargetUserId = x.TargetUserId
                })
            };
        }
    }
}