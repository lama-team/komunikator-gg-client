﻿using System;
using System.Threading.Tasks;
using Client.Contract.Message;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Message;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Message {
    public class SendMessageRequestHandler : BaseHandler<ISendMessageRequest, SendMessageSuccessResponse,
        SendMessageSuccessNotification, SendMessageErrorResponse, SendMessageErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public SendMessageRequestHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(ISendMessageRequest message) {
            SendMessageSuccessResponse response = null;
            try {
                var req = new SendMessageRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Message = new global::Contract.Komunikator.Message {
                        Content = message.Content,
                        MessageId = message.MessageId.ToString(),
                        TargetUserId = message.TargetUserId,
                        Read = false,
                        SourceUserId = ""
                    },
                    Token = message.Token
                };
                response = client.SendMessage(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new SendMessageErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override SendMessageErrorNotification HandleErrorNotification(SendMessageErrorResponse message) {
            return new SendMessageErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override SendMessageSuccessNotification HandleSuccessNotification(SendMessageSuccessResponse message) {
            return new SendMessageSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Content = message?.Message?.Content,
                MessageId = message?.Message?.MessageId,
                TargetUserId = message?.Message?.TargetUserId
            };
        }
    }
}