﻿using System.Threading.Tasks;
using Google.Protobuf;
using MediatR;

namespace Client.Service.Handler {
    public abstract class BaseHandler<TRequest, TSuccessMessage, TSuccessNotification, TErrorMessage,
        TErrorNotification> :
        IBaseHandler<TRequest, TSuccessMessage, TSuccessNotification, TErrorMessage, TErrorNotification>
        where TRequest : IRequest
        where TSuccessMessage : IMessage<TSuccessMessage>
        where TSuccessNotification : INotification
        where TErrorMessage : IMessage<TErrorMessage>
        where TErrorNotification : INotification {
        public abstract Task Handle(TRequest message);

        public TErrorMessage HandleError(byte[] message) {
            dynamic parser = typeof(TErrorMessage).GetProperty("Parser").GetValue(null);
            TErrorMessage result = parser.ParseFrom(message);
            return result;
        }

        public abstract TErrorNotification HandleErrorNotification(TErrorMessage message);

        public TSuccessMessage HandleSuccess(byte[] message) {
            dynamic parser = typeof(TSuccessMessage).GetProperty("Parser").GetValue(null);
            TSuccessMessage result = parser.ParseFrom(message);
            return result;
        }

        public abstract TSuccessNotification HandleSuccessNotification(TSuccessMessage message);
    }
}