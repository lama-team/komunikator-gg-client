﻿using Google.Protobuf;
using MediatR;

namespace Client.Service.Handler {
    public interface ISuccessMessageMapHandler<out TMessage> where TMessage : IMessage<TMessage> {
        TMessage HandleSuccess(byte[] message);
    }

    public interface ISuccessNotificationMapHandler<in TMessage, out TNotification> where TNotification : INotification
        where TMessage : IMessage<TMessage> {
        TNotification HandleSuccessNotification(TMessage message);
    }

    public interface ISuccessMap<TMessage, out TNotification> :
        ISuccessMessageMapHandler<TMessage>,
        ISuccessNotificationMapHandler<TMessage, TNotification>
        where TNotification : INotification
        where TMessage : IMessage<TMessage> {
    }

    public interface IErrorMessageMapHandler<out TMessage> where TMessage : IMessage<TMessage> {
        TMessage HandleError(byte[] message);
    }

    public interface IErrorNotificationMapHandler<in TMessage, out TNotification> where TNotification : INotification
        where TMessage : IMessage<TMessage> {
        TNotification HandleErrorNotification(TMessage message);
    }

    public interface IErrorMap<TMessage, out TNotification> :
        IErrorMessageMapHandler<TMessage>,
        IErrorNotificationMapHandler<TMessage, TNotification>
        where TNotification : INotification
        where TMessage : IMessage<TMessage> {
    }

    public interface IBaseHandler<in TRequest, TSuccessMessage, out TSuccessNotification, TErrorMessage,
        out TErrorNotification> :
        IAsyncRequestHandler<TRequest>,
        ISuccessMap<TSuccessMessage, TSuccessNotification>,
        IErrorMap<TErrorMessage, TErrorNotification> where TRequest : IRequest
        where TSuccessMessage : IMessage<TSuccessMessage>
        where TSuccessNotification : INotification
        where TErrorMessage : IMessage<TErrorMessage>
        where TErrorNotification : INotification {
    }
}