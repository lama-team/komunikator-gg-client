﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Client.Contract.Settings;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Settings;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Settings {
    public class GetSettingsHandler : BaseHandler<IGetSettingsRequest, GetSettingsSuccessResponse,
        GetSettingsSuccessNotification, GetSettingsErrorResponse, GetSettingsErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public GetSettingsHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(IGetSettingsRequest message) {
            GetSettingsSuccessResponse response = null;
            try {
                var req = new GetSettingsRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token
                };
                response = client.GetSettings(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new GetSettingsErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override GetSettingsErrorNotification HandleErrorNotification(GetSettingsErrorResponse message) {
            return new GetSettingsErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override GetSettingsSuccessNotification HandleSuccessNotification(GetSettingsSuccessResponse message) {
            return new GetSettingsSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Settings = message.Settings.ToDictionary(x => x.Name, y => y.Value)
            };
        }
    }
}