﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Client.Contract.Settings;
using Client.Service.Helpers;
using Client.Service.Model.Incoming.Settings;
using Contract.Komunikator;
using MediatR;
using Exception = System.Exception;

namespace Client.Service.Handler.Settings {
    public class SetSettingsHandler : BaseHandler<ISetSettingsRequest, SetSettingsSuccessResponse,
        SetSettingsSuccessNotification, SetSettingsErrorResponse, SetSettingsErrorNotification> {
        private readonly Communication.CommunicationClient client;
        private readonly IMediator mediator;

        public SetSettingsHandler(IMediator mediator, Communication.CommunicationClient client) {
            this.mediator = mediator;
            this.client = client;
        }

        public override async Task Handle(ISetSettingsRequest message) {
            SetSettingsSuccessResponse response = null;
            try {
                var req = new SetSettingsRequest {
                    CorrelationId = message.CorrelationId.ToString(),
                    Token = message.Token
                };
                req.Settings.AddRange(message.Settings.Select(x => new Setting {
                    Name = x.Key,
                    Value = x.Value
                }));
                response = client.SetSettings(req);
                if (!string.IsNullOrEmpty(response.Error)) throw new Exception(response.Error);
                await mediator.Publish(HandleSuccessNotification(response)).ConfigureAwait(false);
            }
            catch (Exception e) {
                Guid g;
                Guid.TryParse(response?.CorrelationId, out g);
                await mediator.Publish(new SetSettingsErrorNotification {
                    CorrelationId = g,
                    Exception = e
                }).ConfigureAwait(false);
            }
        }

        public override SetSettingsErrorNotification HandleErrorNotification(SetSettingsErrorResponse message) {
            return new SetSettingsErrorNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Exception = new Exception(message.Message),
                Status = MessageStatusConverter.Convert(message.Status)
            };
        }

        public override SetSettingsSuccessNotification HandleSuccessNotification(SetSettingsSuccessResponse message) {
            return new SetSettingsSuccessNotification {
                CorrelationId = Guid.Parse(message.CorrelationId),
                Settings = message.Settings.ToDictionary(x => x.Name, x => x.Value)
            };
        }
    }
}