﻿using System;
using Contract.Komunikator;

namespace Client.Service.Helpers
{
    public static class MessageStatusConverter
    {
        public static MessageStatus Convert(Contract.Core.MessageStatus status) {
            switch (status) {
                case Contract.Core.MessageStatus.Ok:
                    return MessageStatus.Ok;
                case Contract.Core.MessageStatus.BadRequest:
                    return MessageStatus.BadRequest;
                case Contract.Core.MessageStatus.Unauthorized:
                    return MessageStatus.Unauthorized;
                case Contract.Core.MessageStatus.NotFound:
                    return MessageStatus.NotFound;
                case Contract.Core.MessageStatus.InternalError:
                    return MessageStatus.InternalError;
                case Contract.Core.MessageStatus.ServiceLayerException:
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
        public static Contract.Core.MessageStatus Convert(MessageStatus status)
        {
            switch (status)
            {
                case MessageStatus.Ok:
                    return Contract.Core.MessageStatus.Ok;
                case MessageStatus.BadRequest:
                    return Contract.Core.MessageStatus.BadRequest;
                case MessageStatus.Unauthorized:
                    return Contract.Core.MessageStatus.Unauthorized;
                case MessageStatus.NotFound:
                    return Contract.Core.MessageStatus.NotFound;
                case MessageStatus.InternalError:
                    return Contract.Core.MessageStatus.InternalError;
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
    }
}
