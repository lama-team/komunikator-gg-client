﻿using System.Collections.Generic;
using Autofac;
using Autofac.Features.Variance;
using Client.Service.Factory;
using Client.Service.Handler;
using Client.Service.Handler.Echo;
using Client.Service.Model.Incoming.Echo;
using Contract.Komunikator;
using Grpc.Core;
using MediatR;

namespace Client.Service.IoC {
    public class Module : Autofac.Module{
        protected override void Load(ContainerBuilder builder) {
            base.Load(builder);
            builder.RegisterType<RouteMap>().AsImplementedInterfaces();
            builder.RegisterType<Router>().AsImplementedInterfaces();
            builder.RegisterType<HandlerFactory>();
            builder.RegisterType<GenericErrorHandler>().AsImplementedInterfaces();
            builder.RegisterInstance(new Channel("127.0.0.1:1025", ChannelCredentials.Insecure)).AsSelf();
            builder.Register(c => new Communication.CommunicationClient(c.Resolve<Channel>())).As<Communication.CommunicationClient>();
            RegisterMediatr(builder);
        }

        private void RegisterMediatr(ContainerBuilder builder) {
            // enables contravariant Resolve() for interfaces with single contravariant ("in") arg
            builder
                .RegisterSource(new ContravariantRegistrationSource());

            // mediator itself
            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            // request handlers
            builder
                .Register<SingleInstanceFactory>(ctx => {
                    var c = ctx.Resolve<IComponentContext>();
                    return t => { object o; return c.TryResolve(t, out o) ? o : null; };
                })
                .InstancePerLifetimeScope();

            // notification handlers
            builder
                .Register<MultiInstanceFactory>(ctx => {
                    var c = ctx.Resolve<IComponentContext>();
                    return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
                })
                .InstancePerLifetimeScope();

            // finally register our custom code (individually, or via assembly scanning)
            // - requests & handlers as transient, i.e. InstancePerDependency()
            // - pre/post-processors as scoped/per-request, i.e. InstancePerLifetimeScope()
            // - behaviors as transient, i.e. InstancePerDependency()
            builder.RegisterAssemblyTypes(ThisAssembly).Except<AsynchronousClient>()
                .AsClosedTypesOf(typeof(IRequestHandler<>)).AsImplementedInterfaces(); // via assembly scan
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IAsyncRequestHandler<>)).AsImplementedInterfaces();
            builder.RegisterType<AsynchronousClient>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<Router>().AsImplementedInterfaces().SingleInstance();
            //builder.RegisterType<MyHandler>().AsImplementedInterfaces().InstancePerDependency();          // or individually
        }
    }
}
