﻿using System;
using System.Collections.Generic;
using Autofac;
using Client.Contract.Core;
using Client.Service.Handler;
using Client.Service.Model.Incoming;
using Client.Service.Model.Incoming.Contact;
using Client.Service.Model.Incoming.Countries;
using Client.Service.Model.Incoming.Echo;
using Client.Service.Model.Incoming.Login;
using Client.Service.Model.Incoming.Message;
using Client.Service.Model.Incoming.Register;
using Client.Service.Model.Incoming.Settings;
using Client.Service.Model.Incoming.User;
using Client.Service.Model.Outgoing;
using Contract.Komunikator;
using MediatR;

namespace Client.Service.Factory
{
    public class HandlerFactory
    {
        private readonly ILifetimeScope scope;

        private Dictionary<string, Type> successMap = new Dictionary<string, Type> {
            {Response.RegisterSuccessResponse, typeof(ISuccessMap<RegisterSuccessResponse, RegisterSuccessNotification>) },
            {Response.LoginSuccessResponse, typeof(ISuccessMap<LoginSuccessResponse, LoginSuccessNotification>) },
            {Response.EchoSuccessResponse, typeof(ISuccessMap<EchoSuccessResponse, EchoSuccessNotification>) },
            {Response.SetUserDataSuccessResponse, typeof(ISuccessMap<SetUserDataSuccessResponse, SetUserDataSuccessNotification>) },
            {Response.ContactRequestSuccessResponse, typeof(ISuccessMap<ContactRequestSuccessResponse, ContactRequestSuccessNotification>) },
            {Response.GetContactsSuccessResponse, typeof(ISuccessMap<GetContactsSuccessResponse, GetContactsSuccessNotification>) },
            {Response.SendMessageSuccessResponse, typeof(ISuccessMap<SendMessageSuccessResponse, SendMessageSuccessNotification>) },
            {Response.GetCountriesSuccessResponse, typeof(ISuccessMap<GetCountriesSuccessResponse, GetCountriesSuccessNotification>) },
            {Response.GetSettingsSuccessResponse, typeof(ISuccessMap<GetSettingsSuccessResponse, GetSettingsSuccessNotification>) },
            {Response.SetSettingsSuccessResponse, typeof(ISuccessMap<SetSettingsSuccessResponse, GetSettingsSuccessNotification>) },
            {Response.ContactRequestNotificationConfirmSuccessResponse, typeof(ISuccessMap<ContactRequestNotificationConfirmSuccessResponse, ContactRequestNotificationConfirmSuccessNotification>) },
            {Response.GetUserStateSuccessResponse, typeof(ISuccessMap<GetUserStateSuccessResponse, GetUserStateSuccessNotification>) }
        };

        private Dictionary<string, Type> errorMap = new Dictionary<string, Type> {
            {Response.RegisterErrorResponse, typeof(IErrorMap<RegisterErrorResponse, RegisterErrorNotification>) },
            {Response.LoginErrorResponse, typeof(IErrorMap<LoginErrorResponse, LoginErrorNotification>) },
            {Response.EchoErrorResponse, typeof(IErrorMap<EchoErrorResponse, EchoErrorNotification>) },
            {Response.SetUserDataErrorResponse, typeof(IErrorMap<SetUserDataErrorResponse, SetUserDataErrorNotification>) },
            {Response.ContactRequestErrorResponse, typeof(IErrorMap<ContactRequestErrorResponse, ContactRequestErrorNotification>) },
            {Response.GetContactsErrorResponse, typeof(IErrorMap<GetContactsErrorResponse, GetContactsErrorNotification>) },
            {Response.SendMessageErrorResponse, typeof(IErrorMap<SendMessageErrorResponse, SendMessageErrorNotification>) },
            {Response.GetCountriesErrorResponse, typeof(IErrorMap<GetCountriesErrorResponse, GetCountriesErrorNotification>) },
            {Response.GetSettingsErrorResponse, typeof(IErrorMap<GetSettingsErrorResponse, GetSettingsErrorNotification>) },
            {Response.SetSettingsErrorResponse, typeof(IErrorMap<SetSettingsErrorResponse, SetSettingsErrorNotification>) },
            {Response.ContactRequestNotificationConfirmErrorResponse, typeof(IErrorMap<ContactRequestNotificationConfirmErrorResponse, ContactRequestNotificationConfirmErrorNotification>) },
            {Response.GetUserStateErrorResponse, typeof(IErrorMap<GetUserStateErrorResponse, GetUserStateErrorNotification>) },
            {Response.GenericError, typeof(IErrorMap<GenericError, GenericErrorNotification>) }

        };

        public HandlerFactory(ILifetimeScope scope) {
            this.scope = scope;
        }
        public INotification Get(BaseReceiveMessage message) {
            if (successMap.ContainsKey(message.MessageType)) {
                dynamic handler = scope.Resolve(successMap[message.MessageType]);
                var proto = handler.HandleSuccess(message.Data);
                var notification = handler.HandleSuccessNotification(proto);
                return notification;
            }
            if (errorMap.ContainsKey(message.MessageType)) {
                dynamic handler = scope.Resolve(errorMap[message.MessageType]);
                var proto = handler.HandleError(message.Data);
                var notification = handler.HandleErrorNotification(proto);
                return notification;
            }
            return null;
        }
    }
}
