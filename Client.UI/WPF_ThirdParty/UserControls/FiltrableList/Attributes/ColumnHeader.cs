﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITA.WPF_ReuseableUserControls.FiltrableList.Attributes
{
    public class ColumnHeader: Attribute
    {
        private ColumnHeader() { }

        public string HeaderText;

        public ColumnHeader(string headerText)
        {
            HeaderText = headerText;
        }
    }
}
