﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITA.WPF_ReuseableUserControls
{
    public class VisibleOnList : Attribute
    {
        public bool IsVisible { get; set; }

        public VisibleOnList() : this(true)
        {
        }

        public VisibleOnList(bool isVisible)
        {
            IsVisible = isVisible;
        }
    }
}
