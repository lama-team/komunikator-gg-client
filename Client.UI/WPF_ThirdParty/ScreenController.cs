﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WPF_ThirdParty.Windows;

namespace WPF_ThirdParty
{
    public class ScreenController
    {
        private BaseWindowWithChangableContent _window;
        private static ScreenController _screenController;

        private ScreenController() { }
        public ScreenController(BaseWindowWithChangableContent window)
        {
            _window = window;
            _screenController = this;
        }

        public static ScreenController Instance
        {
            get
            {
                return _screenController;
            }
        }

        public BaseWindowWithChangableContent Window
        {
            get { return _window; }
        }

        public void SetWindowContent(UserControl userControl)
        {
            _window.SetWindowContent(userControl);
        }

        public void CloseWindow()
        {
            _window.Close();
        }

        public void ShowWindow()
        {
            _window.Show();
        }

        public void ShowDialog()
        {
            _window.ShowDialog();
        }
    }
}
